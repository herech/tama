package net.raiper34.ucoach.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import net.raiper34.ucoach.MilestonesAdapter
import net.raiper34.ucoach.R
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.dataClasses.Milestone

//TODO: Coach can't edit milestones


class GoalFragment : Fragment() {
    private val TAG = "GoalFragment"
    private val db = FirebaseDatabase.getInstance().reference
    private var goal: Goal? = null
    private var role: String = ""
    var goalKey: String? = null
        private set

    val goalName: String?
        get() = (arguments.getSerializable(GOAL) as Goal?)?.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            goal = arguments.getSerializable(GOAL) as Goal
            goalKey = arguments.getString(GOAL_KEY)
            role = arguments.getString(ROLE)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val graph = GraphFragment.newInstance(goalKey)
        val transaction = childFragmentManager.beginTransaction()
        transaction.add(R.id.graphFragmentContainer, graph).commit()

        val chatList = ChatListFragment.newInstance(goalKey, goal, role)
        val chatTransaction = childFragmentManager.beginTransaction()
        chatTransaction.add(R.id.chatListContainer, chatList).commit()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_goal, container, false)

        var milestones: List<Milestone> = ArrayList<Milestone>()

        val adapter = MilestonesAdapter(context, milestones, role == "performer")
        adapter.goalKey = goalKey

        val milestonesView = view.findViewById<ListView>(R.id.milestonesList)
        milestonesView.adapter = adapter

        db.child("milestones").child(goalKey)
                .orderByChild("order")
                //must be single value - otherwise it would set false and true to milestones cykclically and randomly
                .addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get user information
                adapter.clear()
                adapter.addAll(dataSnapshot.children.map {  i ->
                    val retVal = i.getValue(Milestone::class.java)
                    retVal.id = i.key
                    retVal
                })
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Ooops, something went wrong with DB connection.", Toast.LENGTH_SHORT).show()
                Log.e(TAG, databaseError.message)
                //TODO: add firebase logging?
            }
        })

        return view
    }

    companion object {

        private val GOAL = "goal"
        private val GOAL_KEY = "goalKey"
        private val ROLE = "role"

        fun newInstance(goal: Goal, goalKey: String, role: String): GoalFragment {
            val fragment = GoalFragment()
            val args = Bundle()
            args.putSerializable(GOAL, goal)
            args.putString(GOAL_KEY, goalKey)
            args.putString(ROLE, role)
            fragment.arguments = args
            return fragment
        }
    }

}
