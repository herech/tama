package net.raiper34.ucoach.fragments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by mmarusic on 9/4/17.
 */

class GoalsPagerFragment(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private var goalFragments: List<GoalFragment>? = null

    override fun getItem(position: Int): Fragment? {
        return goalFragments?.get(position)
    }

    override fun getCount(): Int {
        return goalFragments!!.size
    }

    fun setGoalFragments(goalFragments: List<GoalFragment>) {
        this.goalFragments = goalFragments
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return goalFragments?.get(position)?.goalName
    }
}
