package net.raiper34.ucoach

import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.util.Log

import com.google.firebase.auth.FirebaseAuth
import net.raiper34.ucoach.activities.PerformerActivity
import net.raiper34.ucoach.activities.SignInActivity

/**
 * Created by mmarusic on 8/26/17.
 */

class MyAuthStateListener(private val activity: FragmentActivity) : FirebaseAuth.AuthStateListener {
    private val TAG = "MyAuthStateListener"
    override fun onAuthStateChanged(firebaseAuth: FirebaseAuth) {
        val user = firebaseAuth.currentUser
        if (user != null) {
            // User is signed in
            var mUsername = user.displayName
            Log.d(TAG, "onAuthStateChanged:signed_in:" + user.uid + " " + mUsername)
            activity.startActivity(Intent(activity, PerformerActivity::class.java))
        } else {
            Log.d(TAG, "onAuthStateChanged:signed_out")
            // Not signed in, launch the Sign In activity
            activity.startActivity(Intent(activity, SignInActivity::class.java))
            activity.finish()
        }
    }
}
