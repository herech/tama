package net.raiper34.ucoach.models

import com.stfalcon.chatkit.commons.models.IUser

/**
 * Created by Filip Gulan on 3.9.2017.
 */

class AuthorChatKit : IUser {

    private var idValue: String
    private var nameValue: String
    private var avatarValue: String

    constructor(id: String, name: String, avatar: String) {
        idValue = id
        nameValue = name
        avatarValue = avatar
    }

    override fun getId(): String {
        return idValue
    }

    override fun getName(): String {
        return nameValue
    }

    override fun getAvatar(): String {
        return avatarValue
    }
}
