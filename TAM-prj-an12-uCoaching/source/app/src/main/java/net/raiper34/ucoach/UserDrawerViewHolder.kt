package net.raiper34.ucoach

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseUser

/**
 * Created by mmarusic on 8/27/17.
 */
class UserDrawerViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val userNameTextView: TextView = v.findViewById(R.id.drawerUserNameTextView)
    private val userEmailTextView: TextView = v.findViewById(R.id.drawerUserEmailTextView)
    private val userPhotoImageView: ImageView = v.findViewById(R.id.drawerUserImageView)

    fun addUserInfo(user: FirebaseUser?) {
        user?.let {
            userNameTextView.text = user.displayName
            userEmailTextView.text = user.email
            Glide.with(userPhotoImageView.context).load(user.photoUrl).into(userPhotoImageView)
        }
    }
}