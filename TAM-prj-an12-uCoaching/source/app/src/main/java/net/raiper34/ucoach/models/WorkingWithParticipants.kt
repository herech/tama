package net.raiper34.ucoach.models

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import net.raiper34.ucoach.dataClasses.Participant


/**
 * Created by Jan Herec on 1.9.2017.
 */
class WorkingWithParticipants {

    private val TAG = "WorkingWithParticipants"

    /**
     * registrace uživatele po jeho přilášení, pokud ještě není registrován
     */
    fun registerParticipant(context : Context) {

        val mFirebaseDatabase = FirebaseDatabase.getInstance().reference
        val mFirebaseAuth = FirebaseAuth.getInstance()
        val user = mFirebaseAuth.currentUser

        // Nejdřív ověříme jestli přihlášený uživatel s daným idečkem už v db není
        mFirebaseDatabase.child("participants").child(user?.uid).addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val currentUser = dataSnapshot.getValue<Participant>(Participant::class.java)

                // Pokud přihlášený uživatel v db není je možné jej registrovat pod jeho idečkem který má ve firebase autentizaci
                if (currentUser == null) {

                    // z objektu FirebaseUser vytvoříme objekt Participant
                    val participant = ParticipantFactory.createFromFirebaseUser(user)
                    participant.token = FirebaseInstanceId.getInstance().token
                    // tento objekt účastníka (Participant) uložíme do db
                    mFirebaseDatabase.child("participants").child(user?.uid).setValue(participant)

                    Toast.makeText(context, "User was registered", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "registerParticipant:onCancelled", databaseError.toException())
                Toast.makeText(context, "Error: Cannot read from firebase", Toast.LENGTH_SHORT).show()

            }
        })

    }
}