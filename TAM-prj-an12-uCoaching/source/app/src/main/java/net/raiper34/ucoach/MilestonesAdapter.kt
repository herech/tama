package net.raiper34.ucoach

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.CompoundButton
import com.google.firebase.database.FirebaseDatabase
import net.raiper34.ucoach.dataClasses.Milestone


/**
 * Created by mmarusic on 9/4/17.
 */
class MilestonesAdapter(context: Context, objects: List<Milestone>, val clickable: Boolean) : ArrayAdapter<Milestone>(context, 0, objects) {
    private val db = FirebaseDatabase.getInstance().reference
    var goalKey : String? = null

    override fun getView(position: Int, convertViewParam: View?, parent: ViewGroup): View? {
        // Check if an existing view is being reused, otherwise inflate the view
        val convertView = convertViewParam ?: LayoutInflater.from(context).inflate(R.layout.milestone_item, parent, false)

        // Lookup view for data population
        val milestoneCheck = convertView?.findViewById<CheckBox>(R.id.milestone_item_check)
        // Get the data item for this position
        val milestone = getItem(position)
        milestone?.let {
            // Populate the data into the template view using the data object
            milestoneCheck?.text = milestone.name
            milestoneCheck?.isChecked = milestone.completed!!
            if (clickable) {
                milestoneCheck?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener
                { _, isChecked ->
                    db.child("milestones").child(goalKey).child(milestone.id).child("completed").setValue(isChecked)
                    Log.d("MILESTONE", milestone.name + " " + isChecked)
                })
            }

        }

        // Return the completed view to render on screen
        return convertView
    }

}
