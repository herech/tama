package net.raiper34.ucoach.models

import com.github.mikephil.charting.utils.ViewPortHandler
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.data.Entry
import java.text.SimpleDateFormat
import java.util.*

class GraphDateValueFormater : IValueFormatter {

    override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        return dateFormat.format(Date(value as Long))
    }
}