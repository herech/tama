package net.raiper34.ucoach.services

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import net.raiper34.ucoach.dataClasses.Participant

/**
 * Created by Filip Gulan on 8.10.2017.
 */
class FirebaseInstanceIDService : FirebaseInstanceIdService() {

    val mFirebaseDatabase = FirebaseDatabase.getInstance().reference
    val mFirebaseAuth = FirebaseAuth.getInstance()

    override fun onTokenRefresh() {
        if (mFirebaseAuth.currentUser?.uid !== null) {
            mFirebaseDatabase.child("participants").child(mFirebaseAuth.currentUser?.uid).addListenerForSingleValueEvent(object : ValueEventListener {

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val currentUser = dataSnapshot.getValue<Participant>(Participant::class.java)
                    currentUser.token = FirebaseInstanceId.getInstance().token
                    mFirebaseDatabase.child("participants").child(mFirebaseAuth.currentUser?.uid).setValue(currentUser)
                }

                override fun onCancelled(p0: DatabaseError?) {
                    Log.d("Error", "Error during refresh token")
                }
            })
        }
    }
}