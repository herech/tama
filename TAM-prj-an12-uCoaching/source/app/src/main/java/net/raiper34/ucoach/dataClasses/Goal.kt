package net.raiper34.ucoach.dataClasses

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

/**
 * Created by Honza Herec on 26.8.2017.
 * Edited by Filip Gulan on 26.8.2017.
 */

@IgnoreExtraProperties
data class Goal(
        var name: String?,
        var completed: Boolean?,
        var coaches: Map<String, Boolean>?,
        var creationDate: Long?,
        var completionDate: Long?,
        var performer: String?,
        var key: String?
) : Serializable {
    constructor() : this(null, null, null, null, null, null, null)
}