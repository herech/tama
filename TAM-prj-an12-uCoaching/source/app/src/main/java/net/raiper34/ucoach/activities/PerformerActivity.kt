package net.raiper34.ucoach.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import net.raiper34.ucoach.GoalEventListener
import net.raiper34.ucoach.R
import net.raiper34.ucoach.UserDrawerViewHolder
import net.raiper34.ucoach.fragments.GoalFragment
import net.raiper34.ucoach.fragments.GoalsPagerFragment

/**
 * Created by mmarusic on 9/1/17.
 * Edited by Jan Herec on 10.9.2017.
 */

class PerformerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private val TAG = "PerformerActivity"
    private val db = FirebaseDatabase.getInstance().reference
    private val mFirebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_performer)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        UserDrawerViewHolder(navigationView.getHeaderView(0)).addUserInfo(mFirebaseAuth.currentUser)

        val button = findViewById<Button>(R.id.youHaveNoGoalsButton)
        button.setOnClickListener {
            createNewGoal()
        }

        db.child("goals").orderByChild("performer").equalTo(mFirebaseAuth.currentUser?.uid)
                .addValueEventListener(GoalEventListener(
                        supportFragmentManager, findViewById(R.id.app_bar_performer_activity)
                ))
    }
    
    private fun createNewGoal() {
        val intent = Intent(this, CreateOrEditGoalActivity::class.java)
        intent.putExtra("CREATE_OR_EDIT_GOAL", "CREATE")
        intent.putExtra("GOAL_ID", "NULL")
        startActivity(intent)
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        when (id) {
            R.id.nav_new_goal -> {
                createNewGoal()
                return true
            }
            R.id.nav_edit_goal -> {
                try {
                    val currentGoalFragment = getCurrentGoalFragment()
                    val intent = Intent(this, CreateOrEditGoalActivity::class.java)
                    intent.putExtra("CREATE_OR_EDIT_GOAL", "EDIT")
                    intent.putExtra("GOAL_ID", currentGoalFragment.goalKey)
                    startActivity(intent)
                    return true
                }
                catch (e : Exception) {
                    Toast.makeText(this@PerformerActivity, "Errro: There is probably no goal to edit!", Toast.LENGTH_LONG).show()
                    return false
                }

            }
            R.id.nav_switch_mode -> {
                startActivity(Intent(this, CoachActivity::class.java))
            }
            R.id.nav_invite -> {
                val intent = Intent(this, CoachRemindersActivity::class.java)
                intent.putExtra("GOAL_ID", "mojeVymysleneId")
                startActivity(intent)

            }
            R.id.nav_sign_out -> {
                mFirebaseAuth.signOut()
                startActivity(Intent(this, SignInActivity::class.java))
                return true
            }
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    /**
     * Funkce vrati aktualni vybrany fragment reprezentujici goal
     */
    fun getCurrentGoalFragment() : GoalFragment {
        val mViewPager: ViewPager = (findViewById<CoordinatorLayout>(R.id.app_bar_performer_activity)).findViewById<ViewPager>(R.id.container)
        val mPagerAdapter = mViewPager.adapter as GoalsPagerFragment

        return mPagerAdapter.getItem(mViewPager.currentItem) as GoalFragment
    }
}
