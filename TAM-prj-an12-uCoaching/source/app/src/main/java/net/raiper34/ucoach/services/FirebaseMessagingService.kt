package net.raiper34.ucoach.services

import android.support.v4.app.NotificationCompat
import net.raiper34.ucoach.R
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.FirebaseMessagingService
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import net.raiper34.ucoach.activities.CoachActivity
import net.raiper34.ucoach.activities.PerformerActivity


/**
 * Created by Filip Gulan on 8.10.2017.
 */
class FirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        val mBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_message_black_24dp)
                .setContentTitle(remoteMessage!!.data["author"])
                .setContentText(remoteMessage!!.data["message"])

        val notificationIntent: Intent
        if (remoteMessage!!.data["role"] == "coach") {
            notificationIntent = Intent(this, PerformerActivity::class.java)
        } else {
            notificationIntent = Intent(this, CoachActivity::class.java)
        }
        val contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        mBuilder.setContentIntent(contentIntent)

        val mNotificationId = Integer.parseInt(remoteMessage!!.data["chat_id"])
        val mNotifyMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotifyMgr.notify(mNotificationId, mBuilder.build())

    }
}