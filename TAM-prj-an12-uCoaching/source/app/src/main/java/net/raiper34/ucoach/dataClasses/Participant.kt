package net.raiper34.ucoach.dataClasses

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable;

/**
 *
 * Created by Filip Gulan on 26.8.2017.
 * Edited by Jan Herec on 1.9.2017
 */

@IgnoreExtraProperties
data class Participant(
    var firstName: String?,
    var lastName: String?,
    var fullName: String?,
    var email: String?,
    var photoUrl: String?,
    var goalsToPerform: Map<String, Boolean>?,
    var goalsToCoach: Map<String, Boolean>?,
    var coachStatus: Map<String, String>?,
    var key: String?,
    var token: String?
) : Serializable {
    constructor() : this(null, null, null, null, null, null, null, null, null, null)
}

