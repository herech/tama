package net.raiper34.ucoach.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import net.raiper34.ucoach.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.dialogs.DialogsList
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import net.raiper34.ucoach.activities.MessageActivity
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.dataClasses.Message
import net.raiper34.ucoach.dataClasses.Participant
import net.raiper34.ucoach.models.AuthorChatKit
import net.raiper34.ucoach.models.DialogChatKit
import net.raiper34.ucoach.models.MessageChatKit
import java.util.*


/**
 * Chat list fragment class
 * @requiredParameters: PARAM_GOAL_ID, PARAM_GOAL, PARAM_ROLE
 * @description: draw chat list
 * @author: Filip Gulan
 * repaired by Jan Herec
 */
class ChatListFragment : Fragment() {

    //Parameters
    private val PARAM_GOAL_ID = "goalId"
    private val PARAM_GOAL = "goal"
    private val PARAM_ROLE = "role"

    //Paths
    private val ROOT_PATH = ""
    private val PARTICIPANTS_PATH = "participants"
    private val LAST_MESSAGES_PATH = "last-messages"

    //Times
    private val TOAST_TIME_ERROR = 5000

    //Attributes
    private var mFirebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    private var goalId = ""
    private var goal: Goal? = null
    private var role = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            goalId = arguments.getString(PARAM_GOAL_ID)
            role = arguments.getString(PARAM_ROLE)
            goal = arguments.getSerializable(PARAM_GOAL) as Goal
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_chat_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        val dbRef = database.getReference(ROOT_PATH)

        //Dialog list set up
        val dialogsList = view?.findViewById<DialogsList>(R.id.dialogsList) as DialogsList
        val dialogsListAdapter = DialogsListAdapter<DialogChatKit>(ImageLoader { imageView, url -> Glide.with(this@ChatListFragment).load(url).into(imageView) })
        dialogsListAdapter.setOnDialogClickListener(object : DialogsListAdapter.OnDialogClickListener<DialogChatKit> {
            override fun onDialogClick(dialog: DialogChatKit) {
                redirectToMessage(dialog, dbRef)
            }
        })
        dialogsList.setAdapter(dialogsListAdapter)

        //Prepare chat partners
        val chatPartners = ArrayList<String>()
        if (role == "coach") {
            chatPartners.add(this.goal?.performer as String)
        } else {
            val tmpGoal = this.goal
            if (tmpGoal != null && tmpGoal.coaches != null) {
                for ((key) in this.goal?.coaches as Map<String, Boolean>) {
                    chatPartners.add(key)
                }
            }
        }

        val dialogArray = ArrayList<DialogChatKit>()
        if (0 < chatPartners.size) { //there is any chat partner left
            this.getChatPartner(0, chatPartners, dialogArray, dialogsListAdapter, dbRef)
        }

    }

    /**
     * Get chat partner
     * Get recursively chat partners from firebase and create dialogs
     */
    private fun getChatPartner(index: Int, chatPartners: ArrayList<String>, dialogArray: ArrayList<DialogChatKit>, dialogsListAdapter: DialogsListAdapter<DialogChatKit>, dbRef: DatabaseReference) {
        dbRef.child(PARTICIPANTS_PATH).child(chatPartners[index]).addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(i: DataSnapshot) {
                        val participant: Participant = i.getValue(Participant::class.java)
                        if (participant != null) {
                            var coachId = ""
                            if (role == "coach") {
                                coachId = mFirebaseAuth.currentUser?.uid as String;
                            } else {
                                coachId = i.key
                            }
                            //Get last messages from firebase
                            dbRef.child(LAST_MESSAGES_PATH).child(goalId).child(coachId).addValueEventListener(
                                    object : ValueEventListener {
                                        override fun onDataChange(j: DataSnapshot) {
                                            val message: Message? = j.getValue(Message::class.java)
                                            val dialog: DialogChatKit
                                            if (message != null) {
                                                var seen = true
                                                if (message.from != (mFirebaseAuth.currentUser?.uid as String)) {
                                                    seen = message.seen as Boolean
                                                }
                                                dialog = createDialog(
                                                        i.key,
                                                        participant.photoUrl as String,
                                                        participant.fullName as String,
                                                        message.content as String,
                                                        message.date as Long,
                                                        seen
                                                )
                                            } else {
                                                dialog = createDialog(
                                                        i.key,
                                                        participant.photoUrl as String,
                                                        participant.fullName as String,
                                                        "",
                                                        Date().time,
                                                        false
                                                )
                                            }
                                            dialogsListAdapter.deleteById(i.key)
                                            dialogsListAdapter.addItem(dialog)
                                            dialogsListAdapter.notifyDataSetChanged()
                                            val nextIndex = index + 1
                                            if (nextIndex < chatPartners.size) { //there is any chat partner left
                                                getChatPartner(nextIndex, chatPartners, dialogArray, dialogsListAdapter, dbRef)
                                            }
                                        }

                                        override fun onCancelled(databaseError: DatabaseError) {
                                            Toast.makeText(activity.applicationContext, "Check your internet connection!", TOAST_TIME_ERROR).show()
                                        }
                                    }
                            )
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Toast.makeText(activity.applicationContext, "Check your internet connection!", TOAST_TIME_ERROR).show()
                    }
                }
        )
    }

    /**
     * Remove dialog with
     * Remove dialog conversation with specific user
     */
    private fun removeDialogWith(dialogArray: ArrayList<DialogChatKit>, name: String): ArrayList<DialogChatKit> {
        var index = 0
        val dialogArrayClone= ArrayList<DialogChatKit>()
        for (dialog in dialogArray) {
            if (dialog.dialogName === name) {
                dialogArrayClone.add(dialog)
            }
            index++
        }
        return dialogArrayClone
    }

    /**
     * Redirect to message
     * Redirect to message activity of clicked chat partner
     */
    private fun redirectToMessage(dialog: DialogChatKit, dbRef: DatabaseReference) {
        dbRef.child(PARTICIPANTS_PATH).child(dialog.id).addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(i: DataSnapshot) {
                        val participant: Participant = i.getValue(Participant::class.java)
                        if (participant != null) {
                            val intent = Intent(getActivity(), MessageActivity::class.java)
                            intent.putExtra("chatPartner", participant)
                            intent.putExtra("chatPartnerId", i.key)
                            intent.putExtra("goalId", goalId)
                            intent.putExtra("goal", goal)
                            intent.putExtra("role", role)
                            startActivity(intent)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Toast.makeText(activity.applicationContext, "Check your internet connection!", TOAST_TIME_ERROR).show()
                    }
                }
        )
    }

    /**
     * Create dialog
     * Create dialog model from given data
     */
    private fun createDialog(id: String, photo: String, name: String, text: String, date: Long, seen: Boolean): DialogChatKit {
        var newMessage = 1
        if (seen) {
            newMessage = 0
        }
        val users = ArrayList<AuthorChatKit>()
        users.add(AuthorChatKit(
                id,
                name,
                ""
        ))
        return DialogChatKit(
                id,
                photo,
                name,
                users,
                MessageChatKit(
                        id,
                        text,
                        AuthorChatKit(
                                id,
                                name,
                                ""
                        ),
                        Date(date)
                ),
                newMessage
        )
    }

    companion object {

        //Parameters
        private val _PARAM_GOAL_ID = "goalId"
        private val _PARAM_GOAL = "goal"
        private val _PARAM_ROLE = "role"

        fun newInstance(goalId: String?, goal: Goal?, role: String): ChatListFragment {
            val fragment = ChatListFragment()
            val args = Bundle()
            args.putString(_PARAM_GOAL_ID, goalId)
            args.putString(_PARAM_ROLE, role)
            args.putSerializable(_PARAM_GOAL, goal)

            fragment.arguments = args
            return fragment
        }
    }

}