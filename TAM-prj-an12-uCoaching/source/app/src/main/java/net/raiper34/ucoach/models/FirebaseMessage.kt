package net.raiper34.ucoach.models
import android.os.AsyncTask
import java.net.HttpURLConnection
import java.net.URL
import java.io.BufferedInputStream

/**
 * Created by raiper34 on 9.10.2017.
 */
 class FirebaseMessage() : AsyncTask<String, Int, Int>() {

    override fun doInBackground(vararg v: String?): Int {
        this.sendMessage(v[0]!!, v[1]!!, v[2]!!, v[3]!!, v[4]!!)
        return 0
    }

    private val SERVER_KEY = "AIzaSyDJTOUP5lCoJzcYVQ0uN7UvRgymCnUvgF4"

    private fun sendMessage(to: String, author: String, chatId: String, message: String, role: String) {
        val url = URL("https://fcm.googleapis.com/fcm/send")
        val connection = url.openConnection() as HttpURLConnection
        connection.requestMethod = "POST"
        connection.setRequestProperty("Content-Type", "application/json")
        connection.setRequestProperty("Authorization", "key=${SERVER_KEY}")

        val json = "{\"to\": \"${to}\",\"data\": {\"role\": \"${role}\",\"author\": \"${author}\",\"chat_id\": \"${chatId}\"\"message\": \"${message}\",}}"
        val os = connection.outputStream
        os.write(json.toByteArray())
        val res = BufferedInputStream(connection.inputStream)
        os.close()

    }

}