package net.raiper34.ucoach.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.stfalcon.chatkit.messages.MessagesList
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.stfalcon.chatkit.messages.MessageInput
import java.util.*
import com.stfalcon.chatkit.commons.ImageLoader
import android.util.TypedValue
import android.widget.*
import net.raiper34.ucoach.R
import com.google.firebase.messaging.FirebaseMessaging
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.dataClasses.Message
import net.raiper34.ucoach.dataClasses.Participant
import net.raiper34.ucoach.dataClasses.Rate
import net.raiper34.ucoach.enums.Feeling
import net.raiper34.ucoach.models.AuthorChatKit
import net.raiper34.ucoach.models.FirebaseMessage
import net.raiper34.ucoach.models.MessageChatKit
import net.raiper34.ucoach.models.Smile
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.widget.EditText



/**
 * Message activity class
 * @description: allow send message with feeling and show last one
 * @author: Filip Gulan
 */
class MessageActivity : AppCompatActivity() {

    //Parameters
    private val PARAM_CHAT_PARTNER = "chatPartner"
    private val PARAM_CHAT_PARTNER_ID = "chatPartnerId"
    private val PARAM_GOAL_ID = "goalId"
    private val PARAM_GOAL = "goal"
    private val PARAM_ROLE = "role"
    //Paths
    private val ROOT_PATH = ""
    private val MESSAGES_PATH = "messages"
    private val LAST_MESSAGES_PATH = "last-messages"
    private val RATES_PATH = "rates"
    //Times
    private val TOAST_TIME = 500
    private val TOAST_TIME_ERROR = 5000
    //Attributes
    private var mFirebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    private var fbMessage = FirebaseMessaging.getInstance()
    private var feeling: Feeling? = null

    /**
     * On Activity create method
     */
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        //Set up
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        //Get params
        val chatPartner = intent.getSerializableExtra(PARAM_CHAT_PARTNER) as Participant
        val chatPartnerId = intent.getSerializableExtra(PARAM_CHAT_PARTNER_ID) as String
        val goalId = intent.getSerializableExtra(PARAM_GOAL_ID) as String
        val goal = intent.getSerializableExtra(PARAM_GOAL) as Goal
        val role = intent.getSerializableExtra(PARAM_ROLE) as String
        val coachId: String?

        if (role == "coach") {
            coachId = mFirebaseAuth.currentUser?.uid
            val feelingsLayout = findViewById<GridLayout>(R.id.feelingsLayout)
            feelingsLayout.visibility = View.GONE
        } else {
            coachId = chatPartnerId
        }

        //Refs
        val dbRef = database.getReference(ROOT_PATH)
        val messagesList = findViewById<MessagesList>(R.id.messagesList)
        val input: MessageInput = findViewById<MessageInput>(R.id.input)
        if (role == "coach") {
            input.inputEditText.hint = "Ask ${chatPartner.firstName} how it is going"
        } else {
            input.inputEditText.hint = "Tell ${chatPartner.firstName} about your progress"
        }

        //Top info
        val chatPartnerImageView = findViewById<ImageView>(R.id.chatPartnerPhoto)
        Glide.with(chatPartnerImageView.context).load(chatPartner.photoUrl).into(chatPartnerImageView)
        (findViewById<TextView>(R.id.chatPartnerNameText)).text = chatPartner.firstName
        (findViewById<TextView>(R.id.goalNameText)).text = goal.name

        // Image loader function for messages
        val imageLoader = ImageLoader { imageView, url -> Glide.with(this@MessageActivity).load(url).into(imageView) }

        //Adapter for message list
        val adapter = MessagesListAdapter<MessageChatKit>(mFirebaseAuth.currentUser?.uid, imageLoader)
        messagesList.setAdapter(adapter)
        adapter.setOnMessageClickListener {
            val i = Intent(this, ChatHistoryActivity::class.java)
            i.putExtra(PARAM_CHAT_PARTNER, chatPartner)
            i.putExtra(PARAM_CHAT_PARTNER_ID, chatPartnerId)
            i.putExtra(PARAM_GOAL_ID, goalId)
            i.putExtra(PARAM_GOAL, goal)
            i.putExtra(PARAM_ROLE, role)
            startActivity(i)
        }

        //Message input
        input.setInputListener(MessageInput.InputListener {
            val inputContent = input.inputEditText.text.toString()
            val currentUserId = mFirebaseAuth.currentUser?.uid.toString()
            val currentUserName = mFirebaseAuth.currentUser?.displayName.toString()
            val currentUserAvatar = mFirebaseAuth.currentUser?.photoUrl.toString()
            val messageId = Date().time.toString();
            val newMessageRef = dbRef.child(MESSAGES_PATH).child(goalId).child(coachId).push()
            newMessageRef.setValue(
                    Message(
                            currentUserId,
                            chatPartnerId,
                            inputContent,
                            this.feeling,
                            null,
                            Date().time,
                            false
                    )
            )
            val newLastMessageRef = dbRef.child(LAST_MESSAGES_PATH).child(goalId).child(coachId)
            newLastMessageRef.setValue(
                    Message(
                            currentUserId,
                            chatPartnerId,
                            inputContent,
                            this.feeling,
                            null,
                            Date().time,
                            false
                    )
            )
            if (role != "coach" && this.feeling != null) {
                val ratesRef = dbRef.child(RATES_PATH).child(goalId).push()
                ratesRef.setValue(
                        Rate(
                                this.feeling,
                                getTodayMidnight()
                        )
                )
            }
            
            FirebaseMessage().execute(chatPartner.token, currentUserName, mFirebaseAuth.currentUser!!.uid!!.hashCode().toString(), inputContent, role)
            true
        })

        //Get last message from firebase
        dbRef.child(LAST_MESSAGES_PATH).child(goalId).child(coachId).addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(i: DataSnapshot) {
                            val message: Message? = i.getValue(Message::class.java)
                        if (message != null) {
                            if (!adapter.isEmpty) {
                                adapter.clear()
                                adapter.notifyDataSetChanged()
                            }
                            if (message.seen == false && message.from != (mFirebaseAuth.currentUser?.uid as String)) {
                                //Set seen to last message
                                message.seen = true
                                dbRef.child(LAST_MESSAGES_PATH).child(goalId).child(coachId).setValue(message)
                            }
                            var smile = ""
                            if (message?.feeling !== null) {
                                smile = Smile.getSmileDependonRole(message?.feeling as Feeling, coachId === message.from as String)
                            }
                            adapter.addToStart(MessageChatKit(
                                    i.key,
                                    message?.content as String + smile,
                                    AuthorChatKit(
                                            message.from as String,
                                            chatPartner.fullName as String,
                                            chatPartner.photoUrl as String
                                    ),
                                    Date(message.date as Long)),
                                    true
                            )
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Toast.makeText(applicationContext, "Check your internet connection!", TOAST_TIME_ERROR).show()
                    }
                }
        )
    }

    private fun getTodayMidnight(): Long {
        val date = GregorianCalendar()
        date.set(Calendar.HOUR_OF_DAY, 0)
        date.set(Calendar.MINUTE, 0)
        date.set(Calendar.SECOND, 0)
        date.set(Calendar.MILLISECOND, 0)
        return date.timeInMillis
    }

    /**
     * Select feeling
     * select and store feeling into property, feeling depend on property
     */
    fun selectFeeling(view: View) {
        val selectedFeeling = view.tag.toString()
        this.resetEmojiTintColor()
        when (selectedFeeling) {
            "NONE" -> {
                this.feeling = Feeling.BEST
                (findViewById<ImageButton>(R.id.smileNone)).setColorFilter(this.fetchAccentColor())
            }
            "BEST" -> {
                this.feeling = Feeling.BEST
                (findViewById<ImageButton>(R.id.smileBest)).setColorFilter(this.fetchAccentColor())
            }
            "GOOD" -> {
                this.feeling = Feeling.GOOD
                (findViewById<ImageButton>(R.id.smileGood)).setColorFilter(this.fetchAccentColor())
            }
            "AVERAGE" -> {
                this.feeling = Feeling.AVERAGE
                (findViewById<ImageButton>(R.id.smileAverage)).setColorFilter(this.fetchAccentColor())
            }
            "BAD" -> {
                this.feeling = Feeling.BAD
                (findViewById<ImageButton>(R.id.smileBad)).setColorFilter(this.fetchAccentColor())
            }
            "WORST" -> {
                this.feeling = Feeling.WORST
                (findViewById<ImageButton>(R.id.smileWorst)).setColorFilter(this.fetchAccentColor())
            }
        }
    }

    /**
     * Reset Emoji Tint Color
     * reset emoji tint coor to default black
     */
    private fun resetEmojiTintColor() {
        (findViewById<ImageButton>(R.id.smileNone)).setColorFilter(Color.BLACK)
        (findViewById<ImageButton>(R.id.smileBest)).setColorFilter(Color.BLACK)
        (findViewById<ImageButton>(R.id.smileGood)).setColorFilter(Color.BLACK)
        (findViewById<ImageButton>(R.id.smileAverage)).setColorFilter(Color.BLACK)
        (findViewById<ImageButton>(R.id.smileBad)).setColorFilter(Color.BLACK)
        (findViewById<ImageButton>(R.id.smileWorst)).setColorFilter(Color.BLACK)
    }

    /**
     * Fetch primary color
     * Function to get primary color, tht are defined in configure "colors.xml" file
     */
    private fun fetchAccentColor(): Int {
        val typedValue = TypedValue()
        theme.resolveAttribute(R.attr.colorAccent, typedValue, true)
        return typedValue.data
    }

    /**
     * Focus input
     * Function to focus input and open keyboard
     */
    fun focusInput(view: View) {
        val input: MessageInput = findViewById<MessageInput>(R.id.input)
        input.requestFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInputFromWindow(view.applicationWindowToken, InputMethodManager.SHOW_FORCED, 0)
    }

    /**
     * Prevent click
     * Function to do not propagate background click
     */
    fun preventClick(view: View) {
        //Empty
    }
}
