package net.raiper34.ucoach.models

import java.util.Date;
import com.stfalcon.chatkit.commons.models.IMessage

/**
 * Created by Filip Gulan on 3.9.2017.
 */

class MessageChatKit : IMessage {

    private var idValue: String
    private var textValue: String
    private var userValue: AuthorChatKit
    private var createdAtValue: Date

    constructor(id: String, text: String, user: AuthorChatKit, createdAt: Date) {
        idValue = id
        textValue = text
        userValue = user
        createdAtValue = createdAt
    }

    override fun getId(): String {
        return idValue
    }

    override fun getText(): String {
        return textValue
    }

    override fun getUser(): AuthorChatKit? {
        return userValue
    }

    override fun getCreatedAt(): Date {
        return createdAtValue
    }
}
