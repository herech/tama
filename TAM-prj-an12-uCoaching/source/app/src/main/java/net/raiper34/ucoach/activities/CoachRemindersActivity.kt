package net.raiper34.ucoach.activities

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.widget.*

import net.raiper34.ucoach.R
import com.google.firebase.database.FirebaseDatabase
import java.util.*
import android.graphics.PorterDuff
import android.app.TimePickerDialog
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import net.raiper34.ucoach.dataClasses.Reminder
import net.raiper34.ucoach.models.ReminderUtils

/**
 * Created by Jan Herec
 */

class CoachRemindersActivity : AppCompatActivity() {

    private val firebaseDbRef = FirebaseDatabase.getInstance().reference
    private val coachId = FirebaseAuth.getInstance().currentUser?.uid // id kouce
    private var goalId : String = "" // id goalu
    private val TAG = "CoachRemindersActivity"
    private var lastReminderIdForCoach : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coach_reminders)

        // ziskame id goalu pro ktery vytvarime remindery
        goalId = intent.getStringExtra("GOAL_ID")
        // ziskame id posledniho eminderu nastaveneho danym koucem
        getLastReminderIdForCoach()
        // pridame z db remindery pro daneho uzivatele a dany goal
        addRemindersToGUI(goalId)
    }

    override fun onStart() {
        super.onStart()
    }


    /**
     * Funkce přidá reminder do GUI na daný index, pokud je paramertr milestone prázdný, potom je přidán prázdný milestone, jinak je přidán milestone s vyplněnými údaji z db
     */
    fun addReminder(reminder: Reminder? = null) {

        // kontejner kam se budou vkladat remindery
        val remindersContainerLinearLayout = findViewById<LinearLayout>(R.id.reminders_container_LinearLayout)

        // pouizijeme sablonu layoutu pro reminder
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val reminderFactoryLinearLayout = inflater.inflate(R.layout.reminder_reusable, null) as LinearLayout

        // cas reminderu
        val reminderTimeTextView = reminderFactoryLinearLayout.findViewById<TextView>(R.id.time_textView)
        val reminderTimeAmPmTextView = reminderFactoryLinearLayout.findViewById<TextView>(R.id.am_pm_textView)

        // switch ktery zapina a vypina reminder
        val reminderStatusSwitch = reminderFactoryLinearLayout.findViewById<Switch>(R.id.reminder_status_switch)

        // jednotlive buttony predstavujici dny v tydnu
        val mondayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.monday_button)
        val tuesdayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.tuesday_button)
        val wednesdayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.wednesday_button)
        val thursdayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.thursday_button)
        val fridayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.friday_button)
        val saturdayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.saturday_button)
        val sundayButton = reminderFactoryLinearLayout.findViewById<Button>(R.id.sunday_button)

        // nastavime time picker dialog
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        val currentHours = calendar.get(Calendar.HOUR)
        val currentMinutes = calendar.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(this@CoachRemindersActivity,
                TimePickerDialog.OnTimeSetListener{

                    view, hourOfDay, minute -> run {
                        if (hourOfDay > 12) {
                            reminderTimeTextView.text = (hourOfDay - 12).toString() + ":" + minute.toString()
                            reminderTimeAmPmTextView.text = "PM"
                        }
                        else {
                            reminderTimeTextView.text = (hourOfDay).toString() + ":" + minute.toString()
                            reminderTimeAmPmTextView.text = "AM"
                        }
                    }
                }, currentHours, currentMinutes, false)


        // vkladame do gui prazdny reminder
        if (reminder == null) {
            reminderFactoryLinearLayout.setTag(R.id.TAG_REMINDER_ID, "NULL")

            // nastavime cas reminderu na aktualni
            val amPmTime = ReminderUtils.getAmPmTimeFromHoursAndMinutes(currentHours.toLong(), currentMinutes.toLong())

            reminderTimeTextView.text = amPmTime[0]
            reminderTimeAmPmTextView.text = amPmTime[1]

            // nastavime switch active na off
            reminderStatusSwitch.isChecked = false

            // nastavime flagu u buttonů
            mondayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
            tuesdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
            wednesdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
            thursdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
            fridayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
            saturdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
            sundayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)

            // nakonec zobrazime tiem picker dialog
            timePickerDialog.show()

        }
        // vkladame do gui milestone z db
        else {
            // nastavime id reminderu
            reminderFactoryLinearLayout.setTag(R.id.TAG_REMINDER_ID, reminder?.key)

            // nastavime atribut cas
            val hours = reminder.hour!!.toLong()
            val minutes = reminder.minute!!.toLong()
            val amPmTime = ReminderUtils.getAmPmTimeFromHoursAndMinutes(hours, minutes)
            reminderTimeTextView.text = amPmTime[0]
            reminderTimeAmPmTextView.text = amPmTime[1]

            // nastavime switch active
            reminderStatusSwitch.isChecked = reminder.active!!

            // nastavime buttony -  jestli jsou aktivni a taky barvicku
            // pokud nejsou akivni nastavime je jako aktivni a pote zavolame funkci toggleDayOfWeekButton, ktera je zneaktivni a naopak
            if (reminder.monday!! < 0) {
                mondayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(mondayButton)
            }
            else {
                mondayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(mondayButton)
            }
            if (reminder.tuesday!! < 0) {
                tuesdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(tuesdayButton)
            }
            else {
                tuesdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(tuesdayButton)
            }
            if (reminder.wednesday!! < 0) {
                wednesdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(wednesdayButton)
            }
            else {
                wednesdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(wednesdayButton)
            }
            if (reminder.thursday!! < 0) {
                thursdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(thursdayButton)
            }
            else {
                thursdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(thursdayButton)
            }
            if (reminder.friday!! < 0) {
                fridayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(fridayButton)
            }
            else {
                fridayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(fridayButton)
            }
            if (reminder.saturday!! < 0) {
                saturdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(saturdayButton)
            }
            else {
                saturdayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(saturdayButton)
            }
            if (reminder.sunday!! < 0) {
                sundayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)
                toggleDayOfWeekButton(sundayButton)
            }
            else {
                sundayButton.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)
                toggleDayOfWeekButton(sundayButton)
            }

        }

        // pri kluknuti na cas zobrazime time picker dialog abysme mohly cas upravit
        reminderTimeTextView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                timePickerDialog.show()
            }
        })

        // pri smazani reminder
        val deleteReminderImageButton = reminderFactoryLinearLayout.findViewById<ImageButton>(R.id.reminder_delete_imageButton)
        deleteReminderImageButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                 // pokud mazeme existujici reminder, tak si jej krome odstraneni z gui take smazeme z db a ze systemu
                 if (reminder != null) {
                     firebaseDbRef.child("reminders").child(coachId).child(goalId).child(reminder.key).removeValue()
                     ReminderUtils.removeAlarmInAlarmManagerForReminderNotification(reminder, this@CoachRemindersActivity)
                 }
                 //Toast.makeText(this@CreateOrEditGoalActivity, "${milestoneId}", Toast.LENGTH_SHORT).show()
                 // samzeme alarm/reminder ze systemu
                 remindersContainerLinearLayout.removeView(reminderFactoryLinearLayout)
                Toast.makeText(this@CoachRemindersActivity, "Goal has been deleted.", Toast.LENGTH_SHORT).show()
                 recreate() //relaunch activity with modifications
            }
        })

        // pri ulozeni reminder
        val saveReminderImageButton = reminderFactoryLinearLayout.findViewById<ImageButton>(R.id.reminder_save_imageButton)
        saveReminderImageButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                // zvysime idecko posledniho reminderu o 7 - tedy alokujeme prostor pro 7 novych idecekm pro 7 dni, ovsem ne vsechny se museji vyuzit nakonec, to zalezi kolik dni celkove si uzivatel pro opakovani reminderu vybral
                firebaseDbRef.child("last-reminder-id-for-coaches").child(coachId).setValue(lastReminderIdForCoach + 7)

                // cas na ktery je nastaven reminder
                val calendar = ReminderUtils.getCalendarTimeFromString(reminderTimeTextView.text.toString() + " " + reminderTimeAmPmTextView.text.toString())

                // pro jednotlive dny ziskame jejich idecka reminderu
                val mondayId = ReminderUtils.getReminderId(mondayButton, lastReminderIdForCoach + 1)
                val tuesdayId = ReminderUtils.getReminderId(tuesdayButton, lastReminderIdForCoach + 2)
                val wednesdayId = ReminderUtils.getReminderId(wednesdayButton, lastReminderIdForCoach + 3)
                val thursdayId = ReminderUtils.getReminderId(thursdayButton, lastReminderIdForCoach + 4)
                val fridayId = ReminderUtils.getReminderId(fridayButton, lastReminderIdForCoach + 5)
                val saturdayId = ReminderUtils.getReminderId(saturdayButton, lastReminderIdForCoach + 6)
                val sundayId = ReminderUtils.getReminderId(sundayButton, lastReminderIdForCoach + 7)

                // reminder ukladany do db
                val dbReminder = Reminder(goalId, calendar.get(Calendar.HOUR_OF_DAY).toLong(), calendar.get(Calendar.MINUTE).toLong(), reminderStatusSwitch.isChecked, mondayId, tuesdayId, wednesdayId, thursdayId, fridayId, saturdayId, sundayId, null)

                // do db vkladame novy reminder
                if (reminder == null) {
                    val newReminderKey = firebaseDbRef.child("reminders").child(coachId).child(goalId).push().key
                    firebaseDbRef.child("reminders").child(coachId).child(goalId).child(newReminderKey).setValue(dbReminder)
                }
                // v db upravujeme existujici reminder
                else {
                    // smazeme puvodne nastavene alarmy a prepiseme je dale novymi
                    ReminderUtils.removeAlarmInAlarmManagerForReminderNotification(reminder, this@CoachRemindersActivity)
                    firebaseDbRef.child("reminders").child(coachId).child(goalId).child(reminder.key).setValue(dbReminder)
                }

                //Toast.makeText(this@CreateOrEditGoalActivity, "${milestoneId}", Toast.LENGTH_SHORT).show()
                // pridame alarm/reminder do systemu, ale jen pokud je aktivni
                if (reminderStatusSwitch.isChecked) {
                    ReminderUtils.createOrUpdateAlarmInAlarmManagerForReminderNotification(dbReminder, this@CoachRemindersActivity)
                }
                Toast.makeText(this@CoachRemindersActivity, "Reminder has been saved.", Toast.LENGTH_SHORT).show()
                recreate() //relaunch activity with modifications
            }
        })


        // ulozime reminder na konec linearLazoutu
        remindersContainerLinearLayout.addView(reminderFactoryLinearLayout)
    }

    /**
     * Funkce zaktivni nebo zdeaktivni tlacitko prezentujici den v tydnu
     */
    fun toggleDayOfWeekButton(button: Button) {
        val isActive = button.getTag(R.id.TAG_REMINDER_IN_DAY_IS_SET) as Boolean
        // zmenime z neakivni na aktivni
        if (!isActive) {
            button.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, true)

            // nastavime barvu
            button.setTextColor(ContextCompat.getColor(getApplicationContext(), android.R.color.white));
            // nastavime pozadi
           // val newDrawable = button.getBackground()
           // DrawableCompat.setTint(newDrawable, android.R.color.holo_blue_dark)
           // ViewCompat.setBackground(button, newDrawable)
            @Suppress("DEPRECATION")
            button.getBackground().setColorFilter(getResources().getColor(android.R.color.holo_blue_dark), PorterDuff.Mode.SRC_ATOP);
        }
        // zmenime z aktivni na neaktivni
        else {
            button.setTag(R.id.TAG_REMINDER_IN_DAY_IS_SET, false)

            // nastavime barvu
            button.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.warm_grey));
            // nastavime pozadi
           // val newDrawable = button.getBackground()
           // DrawableCompat.setTint(newDrawable, R.color.cornflower_blue_light_40)
           // ViewCompat.setBackground(button, newDrawable)
            @Suppress("DEPRECATION")
            button.getBackground().setColorFilter(getResources().getColor(R.color.cornflower_blue_light_40), PorterDuff.Mode.SRC_ATOP);
        }
    }

    /**
     * Funke ziska id posledniho reminderu daneho kouce a ulozi jej do atributu lastReminderIdForCoach
     */
    fun getLastReminderIdForCoach() {
        firebaseDbRef.child("last-reminder-id-for-coaches").child(coachId).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val tmpLastReminderIdForCoach = dataSnapshot.getValue() as Long
                    lastReminderIdForCoach = tmpLastReminderIdForCoach.toInt()
                }
                // pokud neexistuje id posledniho reminderu tak jej vytvorime a nastavime na 0
                else {
                    lastReminderIdForCoach = 0
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@CoachRemindersActivity, "Error. Cannot get last reminder id. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                Log.e(TAG, databaseError.message)
            }
        })
    }

    /**
     * Funkce prida milestony v ramci editovaneho goalu do gui
     */
    fun addRemindersToGUI(goalId : String) {
        firebaseDbRef.child("reminders").child(coachId).child(goalId).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // postupne jednotlive reminder pridavame do gui
                for (singleSnapshot in dataSnapshot.children) {
                    // ziskame reminder
                    val reminder = singleSnapshot.getValue<Reminder>(Reminder::class.java)
                    reminder.key = singleSnapshot.getKey() // ziskame klic pod kterym je reminder ulozen v db

                    addReminder(reminder)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@CoachRemindersActivity, "Error. Cannot load reminders from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                Log.e(TAG, databaseError.message)
            }
        })
    }

    /**
     * Pri kliknuti na nejaky den v tydnu jej bud aktivujeme nebo deaktivujeme podle toho jestli byl aktivovany resp deaktivovany
     */
    fun onClickDayOfWeekButton(v: View) {
        val dayOfWeekButton = v as Button
        toggleDayOfWeekButton(dayOfWeekButton)
    }

    /**
     * Pri kliknuti na tlacitko + pro pridani reminderu
     */
    fun onClickReminderAddImageButton(v: View) {
        addReminder()
    }

}
