package net.raiper34.ucoach.models

import com.stfalcon.chatkit.commons.models.IDialog

/**
 * Created by Filip Gulan on 15.9.2017.
 */
class DialogChatKit: IDialog<MessageChatKit> {

    private var idValue: String
    private var dialogPhotoValue: String
    private var dialogNameValue: String
    private var usersValue: ArrayList<AuthorChatKit>
    private var lastMessageValue: MessageChatKit
    private var unreadCountValue: Int

    constructor(id: String, dialogPhoto: String, dialogName: String, users: ArrayList<AuthorChatKit>,
                lastMessage: MessageChatKit, unreadCount: Int) {
        idValue = id
        dialogPhotoValue = dialogPhoto
        dialogNameValue = dialogName
        usersValue = users
        lastMessageValue = lastMessage
        unreadCountValue = unreadCount
    }

    override fun getId(): String {
        return idValue
    }

    override fun getDialogPhoto(): String {
        return dialogPhotoValue
    }

    override fun getDialogName(): String {
        return dialogNameValue
    }

    override fun getUsers(): ArrayList<AuthorChatKit> {
        return usersValue
    }

    override fun getLastMessage(): MessageChatKit {
        return lastMessageValue
    }

    override fun setLastMessage(lastMessage: MessageChatKit) {
        this.lastMessageValue = lastMessage
    }

    override fun getUnreadCount(): Int {
        return unreadCountValue
    }
}