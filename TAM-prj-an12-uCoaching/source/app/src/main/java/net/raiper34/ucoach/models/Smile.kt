package net.raiper34.ucoach.models

import net.raiper34.ucoach.enums.Feeling


/**
 * Smile class
 * @description: work with UTF emojis
 * @author: Filip Gulan
 */
class Smile {
    companion object {
        const val WORST = "\uD83D\uDE25"
        const val BAD = "\uD83D\uDE41"
        const val AVERAGE = "\uD83D\uDE10"
        const val GOOD = "\uD83D\uDE42"
        const val BEST = "\uD83D\uDE00"

        /**
         * Get emoji
         * Get emoji depend on feeling
         */
        fun getEmoji(feeling: Feeling): String {
            when (feeling) {
                Feeling.BEST -> {
                    return this.BEST
                }
                Feeling.GOOD -> {
                    return this.GOOD
                }
                Feeling.AVERAGE -> {
                    return this.AVERAGE
                }
                Feeling.BAD -> {
                    return this.BAD
                }
                Feeling.WORST -> {
                    return this.WORST
                }
            }
        }

        /**
         * Get emoji
         * Get emoji depend on feeling
         */
        fun getEmoji(feeling: String): String {
            when (feeling) {
                "BEST" -> {
                    return this.BEST
                }
                "GOOD" -> {
                    return this.GOOD
                }
                "AVERAGE" -> {
                    return this.AVERAGE
                }
                "BAD" -> {
                    return this.BAD
                }
                "WORST" -> {
                    return this.WORST
                }
            }
            return ""
        }

        /**
         * Get emoji
         * Get emoji depend on feeling and role
         */
        fun getSmileDependonRole(feeling: Feeling, isCoach: Boolean): String {
            if (isCoach) {
                return ""
            }
            return " ${Smile.getEmoji(feeling)}"
        }

        /**
         * Get emoji
         * Get emoji depend on feeling and role
         */
        fun getSmileDependonRole(feeling: String, isCoach: Boolean): String {
            if (isCoach) {
                return ""
            }
            return " ${Smile.getEmoji(feeling)}"
        }
    }
}