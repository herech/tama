package net.raiper34.ucoach.dataClasses

import com.google.firebase.database.IgnoreExtraProperties
import net.raiper34.ucoach.enums.Feeling

/**
 *
 * Created by Filip Gulan on 26.8.2017.
 * Edited by Jan Herec on 1.9.2017
 */

@IgnoreExtraProperties
data class Message(
        var from: String?,
        var to: String?,
        var content: String?,
        var feeling: Feeling?,
        var fileUrl: String?,
        var date: Long?,
        var seen: Boolean?
) {
    constructor() : this(null, null, null, null, null, null, null)
}