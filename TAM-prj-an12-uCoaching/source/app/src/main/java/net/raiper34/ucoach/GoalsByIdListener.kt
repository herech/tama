package net.raiper34.ucoach

import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.fragments.GoalFragment

class GoalsByIdListener(private val fm: FragmentManager, private val view: View) : ValueEventListener {
    private val db = FirebaseDatabase.getInstance().reference
    private val TAG = "GoalsByIdListener"

    override fun onCancelled(databaseError: DatabaseError) {}

    override fun onDataChange(dataSnapshot: DataSnapshot) {
        val goals: MutableList<GoalFragment> = mutableListOf()
        val goalsPager = GoalsTabsPager()
        val noGoalsView = view.findViewById<TextView>(R.id.youHaveNoGoalsToTrain)
        goalsPager.createTabs(fm, view, goals)

        dataSnapshot.children.forEach { i ->
            Log.d(TAG, i.key)
            Log.d(TAG, i.value.toString())
            Log.d(TAG, i.value.toString())

            db.child("goals").child(i.key).addListenerForSingleValueEvent(
                    object : ValueEventListener {
                        override fun onDataChange(ds: DataSnapshot) {
                            ds.value?.let {
                                goals.add(GoalFragment.newInstance(ds.getValue(Goal::class.java), ds.key, "coach"))
                                goalsPager.mPagerAdapter?.setGoalFragments(goals)
                                goalsPager.notifyDataSetChanged()
                            }
                            if( goals.size > 0 )
                                noGoalsView.visibility = View.INVISIBLE
                        }

                        override fun onCancelled(error: DatabaseError?) {
                            Log.e(TAG, error.toString())
                        }
                    }
            )

        }

        if( goals.size <= 0 )
            noGoalsView.visibility = View.VISIBLE
    }
}