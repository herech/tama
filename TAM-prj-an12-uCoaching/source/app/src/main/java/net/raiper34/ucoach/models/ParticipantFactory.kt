package net.raiper34.ucoach.models

import com.google.firebase.auth.FirebaseUser
import net.raiper34.ucoach.dataClasses.Participant

/**
 * Created by Jan Herec on 2.9.2017.
 */
object ParticipantFactory {
    /**
     * Funkce vytvoří objekt Participant z objektu FirebaseUser
     */
    fun createFromFirebaseUser(user: FirebaseUser?) : Participant {
        var firstName : String? = null
        var lastName : String? = null
        val fullName  = user?.displayName
        val email = user?.email
        val photoUrl = user?.photoUrl

        firstName = ParticipantUtils.getFirstNameFromFullName(fullName)
        lastName = ParticipantUtils.getLastNameFromFullName(fullName)

        // vytvoříme obejkt účastníka z dat poskytnutých firebase o přihlášeném uživateli
        return Participant(firstName, lastName, fullName, email, photoUrl.toString(), null, null, null, null, null)
    }
}