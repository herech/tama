package net.raiper34.ucoach.dataClasses

import com.google.firebase.database.IgnoreExtraProperties

/**
 *
 * Created by Filip Gulan on 26.8.2017.
 * Edited by Jan Herec on 1.9.2017 and 29.9.2017 and 11.10.2017
 */

@IgnoreExtraProperties
data class Reminder(
        //var type: String?, // ReminderType
        var goalId: String?,
        var hour: Long?,
        var minute: Long?,
        var active: Boolean?,

        var monday: Long?, // day: alarmManagerReminderId (pokud alarmManagerReminderId = -1, tak na dany den neni alarm nastaven)
        var tuesday: Long?,
        var wednesday: Long?,
        var thursday: Long?,
        var friday: Long?,
        var saturday: Long?,
        var sunday: Long?,
        var key: String?
) {
    constructor() : this(null, null, null, null, null, null, null, null, null, null, null, null)
}