package net.raiper34.ucoach.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import net.raiper34.ucoach.R
import com.google.firebase.auth.FirebaseAuth
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.stfalcon.chatkit.messages.MessagesList
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.stfalcon.chatkit.messages.MessageInput
import java.util.*
import com.stfalcon.chatkit.commons.ImageLoader
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.dataClasses.Message
import net.raiper34.ucoach.dataClasses.Participant
import net.raiper34.ucoach.enums.Feeling
import net.raiper34.ucoach.models.AuthorChatKit
import net.raiper34.ucoach.models.FirebaseMessage
import net.raiper34.ucoach.models.MessageChatKit
import net.raiper34.ucoach.models.Smile

/**
 * Chat history activity class
 * @description: show all messages and allow send new one
 * @author: Filip Gulan
 */
class ChatHistoryActivity : AppCompatActivity() {

    //Parameters
    private val PARAM_CHAT_PARTNER = "chatPartner"
    private val PARAM_CHAT_PARTNER_ID = "chatPartnerId"
    private val PARAM_GOAL_ID = "goalId"
    private val PARAM_GOAL = "goal"
    private val PARAM_ROLE = "role"
    //Paths
    private val ROOT_PATH = ""
    private val MESSAGES_PATH = "messages"
    private val LAST_MESSAGES_PATH = "last-messages"
    //Times
    private val TOAST_TIME_ERROR = 5000
    //Attributes
    private var mFirebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var database: FirebaseDatabase = FirebaseDatabase.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_history)

        //Set up
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        //Get params
        val chatPartner = intent.getSerializableExtra(PARAM_CHAT_PARTNER) as Participant
        val chatPartnerId = intent.getSerializableExtra(PARAM_CHAT_PARTNER_ID) as String
        val goalId = intent.getSerializableExtra(PARAM_GOAL_ID) as String
        val goal = intent.getSerializableExtra(PARAM_GOAL) as Goal
        val role = intent.getSerializableExtra(PARAM_ROLE) as String
        val coachId: String?

        if (role == "coach") {
            coachId = mFirebaseAuth.currentUser?.uid
        } else {
            coachId = chatPartnerId
        }

        //Refs
        val dbRef = database.getReference(ROOT_PATH)
        val messagesList = findViewById<MessagesList>(R.id.messagesList)
        val input: MessageInput = findViewById<MessageInput>(R.id.input)
        if (role == "coach") {
            input.inputEditText.hint = "Ask ${chatPartner.firstName} how it is going"
        } else {
            input.inputEditText.hint = "Tell ${chatPartner.firstName} about your progress"
        }

        //Top info
        val chatPartnerImageView = findViewById<ImageView>(R.id.chatPartnerPhoto)
        Glide.with(chatPartnerImageView.context).load(chatPartner.photoUrl).into(chatPartnerImageView)
        (findViewById<TextView>(R.id.chatPartnerNameText)).text = chatPartner.firstName
        (findViewById<TextView>(R.id.goalNameText)).text = goal.name

        //Image loader for messages for messages
        val imageLoader = ImageLoader { imageView, url -> Glide.with(this@ChatHistoryActivity).load(url).into(imageView) }

        //Adapter for message list
        val adapter = MessagesListAdapter<MessageChatKit>(mFirebaseAuth.currentUser?.uid, imageLoader)
        messagesList.setAdapter(adapter)

        //Message input
        input.setInputListener(MessageInput.InputListener {
            val inputContent = input.inputEditText.text.toString()
            val currentUserId = mFirebaseAuth.currentUser?.uid.toString()
            val currentUserName = mFirebaseAuth.currentUser?.displayName.toString()
            val currentUserAvatar = mFirebaseAuth.currentUser?.photoUrl.toString()
            val messageId = Date().time.toString();
            val newMessageRef = dbRef.child(MESSAGES_PATH).child(goalId).child(coachId).push()
            newMessageRef.setValue(
                    Message(
                            currentUserId,
                            chatPartnerId,
                            inputContent,
                            null,
                            null,
                            Date().time,
                            false
                    )
            )
            val newLastMessageRef = dbRef.child(LAST_MESSAGES_PATH).child(goalId).child(coachId)
            newLastMessageRef.setValue(
                    Message(
                            currentUserId,
                            chatPartnerId,
                            inputContent,
                            null,
                            null,
                            Date().time,
                            false
                    )
            )

            FirebaseMessage().execute(chatPartner.token, currentUserName, mFirebaseAuth.currentUser!!.uid!!.hashCode().toString(), inputContent, role)
            true
        })

        //Get messages from firebase
        dbRef.child(MESSAGES_PATH).child(goalId).child(coachId).orderByChild("date").addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (!adapter.isEmpty) {
                            adapter.clear()
                            adapter.notifyDataSetChanged()
                        }
                        dataSnapshot.children.forEach { i ->
                            val message: Message = i.getValue(Message::class.java)
                            var smile = ""
                            if (message?.feeling !== null) {
                                smile = Smile.getSmileDependonRole(message?.feeling as Feeling, coachId === message.from as String)
                            }
                            adapter.addToStart(MessageChatKit(
                                    i.key,
                                    message.content as String + smile,
                                    AuthorChatKit(
                                            message.from as String,
                                            chatPartner.fullName as String,
                                            chatPartner.photoUrl as String
                                    ),
                                    Date(message.date as Long)),
                                    true
                            )
                            //val loadingView = findViewById(R.id.loadingLayout) as RelativeLayout
                            //loadingView.setVisibility(View.GONE)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Toast.makeText(getApplicationContext(), "Check your internet connection!", TOAST_TIME_ERROR).show()
                    }
                }
        )
    }
}
