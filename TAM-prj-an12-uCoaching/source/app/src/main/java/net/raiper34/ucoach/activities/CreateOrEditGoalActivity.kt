package net.raiper34.ucoach.activities

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.*
import com.bumptech.glide.Glide
import net.raiper34.ucoach.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.dataClasses.Milestone
import net.raiper34.ucoach.dataClasses.Participant
import net.raiper34.ucoach.enums.CoachStatus

/**
 * Created by Jan Herec on 5.9.2017.
 */

class CreateOrEditGoalActivity : AppCompatActivity() {

    private val firebaseDbRef = FirebaseDatabase.getInstance().reference
    private val TAG = "CreateOrEditGoalActivi"
    private var editedGoal : Goal? = null // aktualne editovany goal, vytahnuty z db na zacatku yivotniho cyklu aktivity
    private var milestonesToDelete : MutableList<String> = mutableListOf<String>() // seznam milestonů (jejich id), které se mají smazat
    private var coachesToDelete : MutableList<String> = mutableListOf<String>() // seznam koucu (jejich id), které se mají smazat
    /**
     * Funkce přidá milestone do GUI na daný index, pokud je paramertr milestone prázdný, potom je přidán prázdný milestone, jinak je přidán milestone s vyplněnými údaji z db
     */
    fun addMilestone(index : Int? = null, milestone: Milestone? = null) {
        // pouizijeme sablonu layoutu pro mileston
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val milestoneFactoryLinearLayout = inflater.inflate(R.layout.milestone_reusable, null) as LinearLayout

        // vkladame do gui prazdny milestone
        if (milestone == null) {
            milestoneFactoryLinearLayout.setTag(R.id.TAG_MILESTONE_ID, "NULL")
        }
        // vkladame do gui milestone z db
        else {
            // nastavime atribut id
            milestoneFactoryLinearLayout.setTag(R.id.TAG_MILESTONE_ID, milestone?.id)
            // nastavime atribut completed
            val completed = milestone.completed
            completed?.let {
                milestoneFactoryLinearLayout.findViewById<CheckBox>(R.id.milestone_checkbox).isChecked = completed
            }
            // nastavime atribut name
            milestone.name?.let {
                milestoneFactoryLinearLayout.findViewById<EditText>(R.id.milestone_name_editText).setText(milestone.name)
            }
        }

        // pri smazani milestonu
        val deleteMilestoneImageButton = milestoneFactoryLinearLayout.findViewById<ImageButton>(R.id.milestone_delete_imageButton)
        deleteMilestoneImageButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val milestoneFactoryLinearLayout = v.parent as LinearLayout
                // pokud mazeme existujici milestone, tak si jej krome odstraneni z gui take poznamename ke smazai z db coz se provede pri stisku tlacitka save
                if (milestone != null) {
                    val milestoneId = milestoneFactoryLinearLayout.getTag(R.id.TAG_MILESTONE_ID) as String
                    milestonesToDelete.add(milestoneId)
                }
                //Toast.makeText(this@CreateOrEditGoalActivity, "${milestoneId}", Toast.LENGTH_SHORT).show()
                val milestoneContainerLinearLayout = milestoneFactoryLinearLayout.parent as LinearLayout
                milestoneContainerLinearLayout.removeView(milestoneFactoryLinearLayout)
            }
        })

        // pri pridani milestonu
        val addMilestoneImageButton = milestoneFactoryLinearLayout.findViewById<ImageButton>(R.id.milestone_add_imageButton)
        addMilestoneImageButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val milestoneFactoryLinearLayout = v.parent as LinearLayout
                val milestoneContainerLinearLayout = milestoneFactoryLinearLayout.parent as LinearLayout
                val index = milestoneContainerLinearLayout.indexOfChild(milestoneFactoryLinearLayout)
                addMilestone(index + 1)
            }
        })

        // ulozime milestone na dany index v linearLazoutu, nebo na konec linearLazoutu pokud neni index specifikovan
        val milestoneContainerLinearLayout = findViewById<LinearLayout>(R.id.milestone_container_LinearLayout)
        if (index == null) {
            milestoneContainerLinearLayout.addView(milestoneFactoryLinearLayout)
        }
        else {
            milestoneContainerLinearLayout.addView(milestoneFactoryLinearLayout, index)
        }
    }

    /**
     * Funkce přidá kouče do GUI, pokud je paramertr coach prázdný, potom je vlozena sablona prazdneho kouce, jinak je přidán kouč s vyplněnými údaji z db
     */
    fun addCoach(coach: Participant? = null) {
        // vytvoříme/přidáme kouče na základě znovupouzitelne sablony
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val coachFactoryLinearLayout = inflater.inflate(R.layout.coach_reusable, null) as LinearLayout

        // pri kliknuti na smazani kouce
        val deleteCoachImageButton = coachFactoryLinearLayout.findViewById<ImageButton>(R.id.coach_delete_imageButton)
        deleteCoachImageButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val coachFactoryLinearLayout = v.parent as LinearLayout
                // pokud mazeme existujiciho kouce, tak si jej krome odstraneni z gui take poznamename ke smazai z db coz se provede pri stisku tlacitka save
                if (coach != null) {
                    val coachId = coachFactoryLinearLayout.getTag(R.id.TAG_COACH_ID) as String
                    coachesToDelete.add(coachId)
                }
                //Toast.makeText(this@CreateOrEditGoalActivity, "${coachId}", Toast.LENGTH_SHORT).show()
                val coachesContainerLinearLayout = coachFactoryLinearLayout.parent as LinearLayout
                coachesContainerLinearLayout.removeView(coachFactoryLinearLayout)
            }
        })

        // prvky z sablony kouce ktere budeme nastavovat popr skryvat podle toho jestli se jedna o editaci nebo tvorbu noveho goalu
        // needitovatelne jmeno kouce
        val coachNameTextView = coachFactoryLinearLayout.findViewById<TextView>(R.id.coach_name_textView)
        // editovatelen jmeno kouce
        val coachNameEditText = coachFactoryLinearLayout.findViewById<TextView>(R.id.coach_name_editText)
        // info o stavu zadosti o koucovani
        val coachStatusTextView = coachFactoryLinearLayout.findViewById<TextView>(R.id.coachStatus_textView)
        // email kouce
        val coachEmailTextView = coachFactoryLinearLayout.findViewById<TextView>(R.id.coach_email_textView)
        // info o existenci nebo neexistenci zadaneho kouce
        val coachExistTextView = coachFactoryLinearLayout.findViewById<TextView>(R.id.coach_exist_textView)
        // obrazek kouce
        val coachNameImageView = coachFactoryLinearLayout.findViewById<ImageView>(R.id.coach_photo_imageView)

        // pokud je coach null, tedy vkladame sablonu prazdneho kouce do gui
        if (coach == null) {
            // idecko kouce je prozatim null
            coachFactoryLinearLayout.setTag(R.id.TAG_COACH_ID, "NULL")
            // odstranime/skryjeme polozky z gui ktere nechceme aby byli nyni videt
            coachNameTextView.visibility = View.GONE
            coachStatusTextView.visibility = View.GONE
            coachEmailTextView.text = ""
            coachExistTextView.text = ""

            // při zadávání jména kouče se tento kouč hledá a pokud se najde vyplní se v GUI
            val coachNameEditText = coachFactoryLinearLayout.findViewById<EditText>(R.id.coach_name_editText)
            coachNameEditText.addTextChangedListener(object : TextWatcher {

                // the user's changes are saved here
                override fun onTextChanged(c: CharSequence, start: Int, before: Int, count: Int) {
                    // spustime v jinem vlakne nez bezi ui hledani kouce zadaneho uzivatelem
                    Thread(Runnable {
                       try {
                            val mFirebaseDatabase = FirebaseDatabase.getInstance().reference
                            // Najdeme kouce s danym jmenem
                            mFirebaseDatabase.child("participants").orderByChild("fullName").equalTo(c.toString()).addListenerForSingleValueEvent(object : ValueEventListener {

                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    // Pokud kouc je v db oznamime to uzivateli
                                    if (dataSnapshot.exists()) {

                                        // pod stejnym jemnem muye byt koucu vice, yatim to neresme  vratme posledniho
                                        for (singleSnapshot in dataSnapshot.children) {
                                            // ziskame kouce
                                            val searchedCoach = singleSnapshot.getValue<Participant>(Participant::class.java)
                                            searchedCoach.key = singleSnapshot.getKey() // ziskame klic pod kterym je kouc ulozen v db

                                            //update ui on UI thread
                                            runOnUiThread {
                                                coachExistTextView.visibility = View.GONE
                                                coachEmailTextView.visibility = View.VISIBLE
                                                coachEmailTextView.text = searchedCoach.email
                                                Glide.with(coachNameImageView.context).load(searchedCoach.photoUrl).into(coachNameImageView)
                                                // idecko kouce nastavime
                                                coachFactoryLinearLayout.setTag(R.id.TAG_COACH_ID, searchedCoach.key)
                                            }
                                        }
                                    }
                                    // Pokud kouc neni v db oznamime to uzivateli
                                    else {
                                        //update ui on UI thread
                                        runOnUiThread {
                                            coachEmailTextView.visibility = View.GONE
                                            coachExistTextView.visibility = View.VISIBLE
                                            coachExistTextView.text = "Couch not found"
                                            coachNameImageView.setImageResource(android.R.drawable.sym_def_app_icon)
                                            // idecko kouce je prozatim null
                                            coachFactoryLinearLayout.setTag(R.id.TAG_COACH_ID, "NULL")
                                        }
                                    }
                                }

                                override fun onCancelled(databaseError: DatabaseError) {
                                    // Getting Post failed, log a message
                                    Log.w(TAG, "addCoach:OnTextChanged:onCancelled", databaseError.toException())
                                    runOnUiThread {
                                        Toast.makeText(this@CreateOrEditGoalActivity, "error retirieving coach from db",
                                                Toast.LENGTH_SHORT).show()
                                     }
                                    //Toast.makeText(this@CreateOrEditGoalActivity, "Error: Cannot read from firebase", Toast.LENGTH_SHORT).show()

                                }
                            })
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }).start()
                }

                override fun beforeTextChanged(c: CharSequence, start: Int, count: Int, after: Int) {
                    // this space intentionally left blank
                }

                override fun afterTextChanged(c: Editable) {
                    // this one too
                }
            })
        }
        // pokud neni coach null, tedy vkladame kouce z db do gui
        else {
            // nastavime idecko kouce
            coachFactoryLinearLayout.setTag(R.id.TAG_COACH_ID, coach.key)
            // odstranime/skryjeme polozky z gui ktere nechceme aby byli nyni videt
            coachNameEditText.visibility = View.GONE
            coachExistTextView.visibility = View.GONE

            // nastavíme údaje o koučovi
            coachEmailTextView.text = coach.email
            coachNameTextView.text = coach.fullName

            // pokud kouč nemá žádné goaly ke koučování nebo nemá ke koučování goal, který by měl mít, tak je v db nějaká nekonzistence!!
            if (coach.coachStatus == null || coach.coachStatus!!.get(editedGoal!!.key) == null) {
                Toast.makeText(this@CreateOrEditGoalActivity, "Inconsistent db for coach ${coach?.key} coaching goal: ${editedGoal?.key}", Toast.LENGTH_SHORT).show()
            }
            // db by měla být konzistentní, nastavíme status kouče
            else {
                // ziskame goal status
                val coachStatus = coach.coachStatus!!.get(editedGoal!!.key)!!
                // na zaklade stavu prijeti nebo neprijeti goalu koucem upravime info v GUI
                when (CoachStatus.valueOf(coachStatus)) {
                    CoachStatus.REQUESTED -> {
                        coachStatusTextView.text = "Coach requested"
                        coachStatusTextView.setTextColor(android.R.color.holo_orange_light)

                        coachStatusTextView.setTextColor( ContextCompat.getColor(getApplicationContext(),android.R.color.holo_orange_light));

                    }
                    CoachStatus.APPROVED -> {
                        coachStatusTextView.text = "Approved by coach"
                        coachStatusTextView.setTextColor( ContextCompat.getColor(getApplicationContext(),android.R.color.holo_green_light));

                    }
                    CoachStatus.DECLINED -> {
                        coachStatusTextView.text = "Declined by coach"
                        coachStatusTextView.setTextColor( ContextCompat.getColor(getApplicationContext(),android.R.color.holo_red_light));

                    }
                }
            }

            // nastavime fotku kouce
            Glide.with(coachNameImageView.context).load(coach.photoUrl).into(coachNameImageView)
        }

        // do obaloveho laypoutu pro kouce pridame kouce
        val coachesContainerLinearLayout = findViewById<LinearLayout>(R.id.coaches_container_LinearLayout)
        coachesContainerLinearLayout.addView(coachFactoryLinearLayout)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_or_edit_goal)

        // zjistime jestli budeme v teto aktivite upravovat nejaky goal nebo vytvaret zbrusu novy
        val createOrEditGoal = intent.getStringExtra("CREATE_OR_EDIT_GOAL")
        when (createOrEditGoal) {
            "CREATE" -> {
                addMilestone(0)
                addCoach()
            }
            "EDIT" -> {
                // ziskame id goalu ktery upravujeme
                val goalId = intent.getStringExtra("GOAL_ID")
                // na zaklade daneho id ziskame objekt goalu z db ktery si ulozime a nastavime podle nej nazev editovanego goalu v gui
                createAndSaveGoalInstanceAndAddGoalNameToGUI(goalId)
                // pridame do gui milestony spadajici pod tento goal
                addMilestonesToGUI(goalId)
                // pridame do gui kouce spadajici pod tento goal
                addCoachesToGUI(goalId)
                //Toast.makeText(this@CreateOrEditGoalActivity, "${goalId}", Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Ulozime goal nebo jeho zmeny do db
     */
    fun onButtonClickSave(view: View) {

        val mFirebaseDatabase = FirebaseDatabase.getInstance().reference

        // ziskame info o goalu
        val performerId = FirebaseAuth.getInstance().currentUser?.uid // id performera
        val goalCompleted = false // dokonceni
        val creationDate = System.currentTimeMillis() // datum vytvoreni
        val goalNameEditText = findViewById<EditText>(R.id.goal_name_editText)
        val name = goalNameEditText.text.toString() // jmeno goalu

        // ziskame kouce
        val coaches :MutableMap<String, Boolean> = mutableMapOf() //kouci

        val coachesContainerLinearLayout = findViewById<LinearLayout>(R.id.coaches_container_LinearLayout)
        for (i in 0..coachesContainerLinearLayout.childCount - 1) {
            val coachId = coachesContainerLinearLayout.getChildAt(i).getTag(R.id.TAG_COACH_ID).toString()
            if (coachId != "NULL") {
                coaches.put(coachId, true)
            }
        }

        // ziskame milestony
        val newMilestones : MutableList<Milestone> = mutableListOf<Milestone>() // nove pridane milestony
        val oldMilestones : MutableList<Milestone> = mutableListOf<Milestone>() // nove pridane milestony

        val milestoneContainerLinearLayout = findViewById<LinearLayout>(R.id.milestone_container_LinearLayout)
        var milestoneOrder = 0L // poradi milestonu v ramci skupiny milestonu
        for (i in 0..milestoneContainerLinearLayout.childCount - 1) {
            val milestone = milestoneContainerLinearLayout.getChildAt(i) as LinearLayout
            val milestoneId = milestone.getTag(R.id.TAG_MILESTONE_ID).toString()
            // old milestone
            if (milestoneId != "NULL") {
                val milestoneCompleted = milestone.findViewById<CheckBox>(R.id.milestone_checkbox).isChecked
                val milestoneName = milestone.findViewById<EditText>(R.id.milestone_name_editText).text.toString()
                val oldMilestone = Milestone(milestoneName, null, milestoneCompleted, null, milestoneOrder, milestoneId)
                oldMilestones.add(oldMilestone)


            }
            // new milestone
            else {
                val milestoneCompleted = milestone.findViewById<CheckBox>(R.id.milestone_checkbox).isChecked
                val milestoneName = milestone.findViewById<EditText>(R.id.milestone_name_editText).text.toString()
                val newMilestone = Milestone(milestoneName, System.currentTimeMillis(), milestoneCompleted, null, milestoneOrder, null)
                newMilestones.add(newMilestone)
            }
            milestoneOrder++
        }

        // zjistime jestli v teto aktivite upravujeme nejaky goal nebo vytvarime zbrusu novy a podle toho goal ulozime nebo zmenime
        val createOrEditGoal = intent.getStringExtra("CREATE_OR_EDIT_GOAL")
        when (createOrEditGoal) {
            "CREATE" -> {

                // ulozime goal
                val goal = Goal(name, goalCompleted, coaches, creationDate, null, performerId, null)
                val goalKey = mFirebaseDatabase.child("goals").push().key
                mFirebaseDatabase.child("goals").child(goalKey).setValue(goal)

                // ulozime nove milestony
                for (milestone in newMilestones) {
                    val milestoneKey = mFirebaseDatabase.child("milestones").child(goalKey).push().key
                    mFirebaseDatabase.child("milestones").child(goalKey).child(milestoneKey).setValue(milestone)
                }

                // ulozime do performera info onovem goalu
                mFirebaseDatabase.child("participants").child(performerId).child("goalsToPerform").child(goalKey).setValue(true)

                // ulozime do koucu info onovem goalu
                for (coach in coaches) {
                    mFirebaseDatabase.child("participants").child(coach.key).child("goalsToCoach").child(goalKey).setValue(true)
                    mFirebaseDatabase.child("participants").child(coach.key).child("coachStatus").child(goalKey).setValue(CoachStatus.REQUESTED)
                }

            }
            "EDIT" -> {

                val editedGoal = editedGoal
                val goalKey = editedGoal?.key

                // aktualizujeme goal
                mFirebaseDatabase.child("goals").child(goalKey).child("name").setValue(name)

                // ulozime nove milestony
                for (milestone in newMilestones) {
                    val milestoneKey = mFirebaseDatabase.child("milestones").child(goalKey).push().key
                    mFirebaseDatabase.child("milestones").child(goalKey).child(milestoneKey).setValue(milestone)
                }
                // aktualizujeme existujici milestony
                for (milestone in oldMilestones) {
                    //val milestoneDbRef = mFirebaseDatabase.child("milestones").child(goalKey).child(milestone.id)
                    mFirebaseDatabase.child("milestones").child(goalKey).child(milestone.id).child("completed").setValue(milestone.completed)
                    mFirebaseDatabase.child("milestones").child(goalKey).child(milestone.id).child("name").setValue(milestone.name)
                    mFirebaseDatabase.child("milestones").child(goalKey).child(milestone.id).child("order").setValue(milestone.order)
                }

                // smazeme smazane milestony
                for (milestoneId in milestonesToDelete) {
                    mFirebaseDatabase.child("milestones").child(goalKey).child(milestoneId).removeValue()
                }

                // ulozime do koucu info onovem goalu a taky do golu info o novych koucich
                for (coach in coaches) {
                    mFirebaseDatabase.child("participants").child(coach.key).child("goalsToCoach").child(goalKey).setValue(true)
                    mFirebaseDatabase.child("participants").child(coach.key).child("coachStatus").child(goalKey).setValue(CoachStatus.REQUESTED)

                    mFirebaseDatabase.child("goals").child(goalKey).child("coaches").child(coach.key).setValue(true)
                }

                // smazeme smazane kouce a zpravy s nimi souvisejici
                for (coachId in coachesToDelete) {
                    mFirebaseDatabase.child("participants").child(coachId).child("goalsToCoach").child(goalKey).removeValue()
                    mFirebaseDatabase.child("participants").child(coachId).child("coachStatus").child(goalKey).removeValue()
                    mFirebaseDatabase.child("goals").child(goalKey).child("coaches").child(coachId).removeValue()

                    mFirebaseDatabase.child("last-messages").child(goalKey).child(coachId).removeValue()
                }

            }
        }

        Toast.makeText(this@CreateOrEditGoalActivity, "Saved", Toast.LENGTH_LONG).show()

        //ReminderUtils.createOrUpdateAlarmInAlarmManagerForReminderNotification(Reminder("DAILY", 4, 32, 0, null), this@CreateOrEditGoalActivity)
        //ReminderUtils.createOrUpdateAlarmInAlarmManagerForReminderNotification(Reminder("SATURDAY", 5, 5, 0, null), this@CreateOrEditGoalActivity)
        //ReminderUtils.removeAlarmInAlarmManagerForReminderNotification(0, this@CreateOrEditGoalActivity)

        finish()
    }

    /**
     * Funkce vytvori a uloyi instanci objektu Goal na zaklade id tohoto goalu v db, a prida nazev editovanem goalu do gui
     */
    fun createAndSaveGoalInstanceAndAddGoalNameToGUI(goalId : String) {
        // ziskame goal na zaklade jeho id z db
        FirebaseDatabase.getInstance().reference.child("goals").child(goalId).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // ulozime si vztvoreny objekt goalu
                val goal = dataSnapshot.getValue<Goal>(Goal::class.java)
                goal.key = goalId
                editedGoal = goal
                // nastavime nazev goalu
                val goalNameEditText = findViewById<EditText>(R.id.goal_name_editText)
                goalNameEditText.setText(goal?.name)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@CreateOrEditGoalActivity, "Error. Cannot load goal from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                Log.e(TAG, databaseError.message)
            }
        })
    }

    /**
     * Funkce prida milestony v ramci editovaneho goalu do gui
     */
    fun addMilestonesToGUI(goalId : String) {
        firebaseDbRef.child("milestones").child(goalId).orderByChild("order").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                var currentIndex = 0 // index aktualniho milestonu
                // postupne jednotlive milestony pridavame do gui
                for (singleSnapshot in dataSnapshot.children) {
                    // ziskame mileston
                    val milestone = singleSnapshot.getValue<Milestone>(Milestone::class.java)
                    milestone.id = singleSnapshot.getKey() // ziskame klic pod kterym je milestone ulozen v db

                    addMilestone(currentIndex, milestone)
                    currentIndex++
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@CreateOrEditGoalActivity, "Error. Cannot load milestones from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                Log.e(TAG, databaseError.message)
            }
        })
    }

    /**
     * Funkce prida kouce v ramci editovaneho goalu do gui
     */
    fun addCoachesToGUI(goalId : String) {
        // nejdriv zjistime idecka koucu daneho goalu
        firebaseDbRef.child("goals").child(goalId).child("coaches").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                // postupne projdeme idecka koucu daneho goalu
                for (singleSnapshot in dataSnapshot.children) {
                    // ziskame id kouče
                    val coachId = singleSnapshot.getKey()

                    // podle id kouce jej vytahneme z db a pridame je do gui
                    firebaseDbRef.child("participants").child(coachId).addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                                // ziskame kouče
                                val coach = dataSnapshot.getValue<Participant>(Participant::class.java)
                                coach.key = coachId
                                // kouce pridame do gui
                                addCoach(coach)
                        }

                        override fun onCancelled(databaseError: DatabaseError) {
                            Toast.makeText(this@CreateOrEditGoalActivity, "Error. Cannot load goal coach from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                            Log.e(TAG, databaseError.message)
                        }
                    })
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@CreateOrEditGoalActivity, "Error. Cannot load goal coaches from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                Log.e(TAG, databaseError.message)
            }
        })
    }

    /**
     * Funkce prida empty kouce do GUI
     */
    fun onButtonClickAddCoach(view: View) {
        addCoach()
    }

    /**
     * Funkce prida empty milestone do GUI
     */
    fun onButtonClickAddMilestone(view: View) {
        addMilestone()
    }
}
