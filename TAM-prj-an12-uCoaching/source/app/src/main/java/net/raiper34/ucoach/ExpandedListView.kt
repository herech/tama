package net.raiper34.ucoach

/**
 * Created by raiper34 on 10.12.2017.
 */

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.widget.ListView

class ExpandedListView(context: Context, attrs: AttributeSet) : ListView(context, attrs) {
    private var params: android.view.ViewGroup.LayoutParams? = null
    private var oldCount = 0

    override fun onDraw(canvas: Canvas) {
        if (count != oldCount && count != 0) {
            val height = getChildAt(0).height + 1
            oldCount = count
            params = layoutParams
            params!!.height = count * height
            layoutParams = params
        }

        super.onDraw(canvas)
    }
}