package net.raiper34.ucoach.dataClasses

import com.google.firebase.database.IgnoreExtraProperties
import net.raiper34.ucoach.enums.Feeling

/**
 *
 * Created by Filip Gulan on 26.8.2017.
 * Edited by Jan Herec on 1.9.2017
 */

@IgnoreExtraProperties
data class Rate(
        var feeling: Feeling?,
        var date: Long?
) {
    constructor() : this(null, null)
}
