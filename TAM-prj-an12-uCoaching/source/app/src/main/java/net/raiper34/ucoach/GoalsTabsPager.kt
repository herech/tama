package net.raiper34.ucoach

import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.view.View
import net.raiper34.ucoach.fragments.GoalFragment
import net.raiper34.ucoach.fragments.GoalsPagerFragment

/**
 * Created by mmarusic on 10/16/17.
 */

class GoalsTabsPager {
    var mPagerAdapter: GoalsPagerFragment? = null

    fun createTabs(fm: FragmentManager, view: View, goals: List<GoalFragment>) {
        // Create the adapter that will return a fragment for each section
        mPagerAdapter = GoalsPagerFragment(fm)
        mPagerAdapter?.setGoalFragments(goals)

        // Set up the ViewPager with the sections adapter.
        val mViewPager: ViewPager = view.findViewById(R.id.container)
        mViewPager.offscreenPageLimit = 10

        mViewPager.adapter = mPagerAdapter
        //TODO: najdi posledne pouzite ?
        mViewPager.currentItem = 0

        val tabLayout: TabLayout = view.findViewById(R.id.tab_layout)
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        tabLayout.setupWithViewPager(mViewPager)
    }

    fun notifyDataSetChanged(){
        mPagerAdapter?.notifyDataSetChanged()
    }

}
