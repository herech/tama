package net.raiper34.ucoach.enums

/**
 * Created by Filip Gulan on 26.8.2017.
 */

enum class CoachStatus {
    REQUESTED, WAITING, DECLINED, APPROVED
}
