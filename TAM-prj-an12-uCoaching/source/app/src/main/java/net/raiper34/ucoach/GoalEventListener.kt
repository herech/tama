package net.raiper34.ucoach


import android.support.v4.app.FragmentManager
import android.view.View
import android.widget.Button
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.fragments.GoalFragment

/**
 * Created by mmarusic on 9/4/17.
 */

class GoalEventListener(private val fm: FragmentManager, private val view: View) : ValueEventListener {

    override fun onDataChange(dataSnapshot: DataSnapshot) {
        // Get user information
        val goals: List<GoalFragment> = dataSnapshot.children.map { i ->
            GoalFragment.newInstance(i.getValue(Goal::class.java), i.key, "performer")
        }

        if(goals.isNotEmpty()) {
            val button = view.findViewById<Button>(R.id.youHaveNoGoalsButton)
            button.visibility = View.INVISIBLE
            val goalsPager = GoalsTabsPager()
            goalsPager.createTabs(fm, view, goals)
        } else {
            val button = view.findViewById<Button>(R.id.youHaveNoGoalsButton)
            button.visibility = View.VISIBLE
        }
    }

    override fun onCancelled(databaseError: DatabaseError) {}
}
