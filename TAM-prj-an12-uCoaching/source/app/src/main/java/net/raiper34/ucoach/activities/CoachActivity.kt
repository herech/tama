package net.raiper34.ucoach.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import net.raiper34.ucoach.R
import android.widget.Toast
import net.raiper34.ucoach.GoalsByIdListener
import net.raiper34.ucoach.UserDrawerViewHolder
import net.raiper34.ucoach.fragments.GoalFragment
import net.raiper34.ucoach.fragments.GoalsPagerFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

/**
 * Created by mmarusic on 9/15/17.
 * Slightly edited by Jan Herec on 24/11/2017
 */
class CoachActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val TAG = "CoachActivity"
    private val db = FirebaseDatabase.getInstance().reference
    private val mFirebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coach)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        UserDrawerViewHolder(navigationView.getHeaderView(0)).addUserInfo(mFirebaseAuth.currentUser)

        db.child("participants").child(mFirebaseAuth.currentUser?.uid).child("coachStatus").orderByValue().equalTo("REQUESTED")
                .addValueEventListener(GoalsByIdListener(supportFragmentManager, findViewById(R.id.app_bar_performer_activity)))


    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        when (id) {
            R.id.nav_switch_mode -> {
                startActivity(Intent(this, PerformerActivity::class.java))
            }
            R.id.nav_add_reminder -> {
                try {
                    val currentGoalFragment = getCurrentGoalFragment()
                    val intent = Intent(this, CoachRemindersActivity::class.java)
                    intent.putExtra("GOAL_ID", currentGoalFragment.goalKey)
                    startActivity(intent)
                }
                catch (e : Exception) {
                    Toast.makeText(this@CoachActivity, "Errro: There is probably no goal to which it would be possible to create reminder!", Toast.LENGTH_LONG).show()
                    return false
                }
                return true
            }
            R.id.nav_invite -> {

            }
            R.id.nav_sign_out -> {
                mFirebaseAuth.signOut()
                startActivity(Intent(this, SignInActivity::class.java))
                return true
            }
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    /**
     * Funkce vrati aktualni vybrany fragment reprezentujici goal
     */
    fun getCurrentGoalFragment() : GoalFragment {
        val mViewPager: ViewPager = (findViewById<CoordinatorLayout>(R.id.app_bar_performer_activity)).findViewById<ViewPager>(R.id.container)
        val mPagerAdapter = mViewPager.adapter as GoalsPagerFragment

        return mPagerAdapter.getItem(mViewPager.currentItem) as GoalFragment
    }
}


