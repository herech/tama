package net.raiper34.ucoach

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build


/**
 * Created by Jan Herec on 11.10.2017.
 */
/**
 * Container for methods, that should be called (not necessarily from same place in batch) when application is launched
 */
object OnApplicationLaunch {
    fun createNotificationChannels(context: Context) {
        // only on api >= 26 we can use notification channels
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // CREATE CHANNEL FOR COACH REMINDERS
            val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channelId = context.getString(R.string.coach_reminder_channel_id)
            val channelName = context.getString(R.string.coach_reminder_channel_name)
            val notificationChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)

            // Notification channel configuration
            notificationChannel.description = context.getString(R.string.coach_reminder_channel_description)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(true)
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            // final creating of channel
            mNotificationManager.createNotificationChannel(notificationChannel)
        }
    }
}