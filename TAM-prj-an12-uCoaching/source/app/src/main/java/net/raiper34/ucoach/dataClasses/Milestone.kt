package net.raiper34.ucoach.dataClasses

import com.google.firebase.database.IgnoreExtraProperties

/**
 *
 * Created by Filip Gulan on 26.8.2017.
 * Edited by Jan Herec on 1.9.2017
 */

@IgnoreExtraProperties
data class Milestone(
        var name: String?,
        var creationDate: Long?,
        var completed: Boolean?,
        var completionDate: Long?,
        var order: Long?,
        var id: String?  // toto se neuklada do firebase ale zde si pripadne ulozime klic pod kterym je zaznam v db ulozen
) {
    constructor() : this(null, null, null, null, null, null)
}
