package net.raiper34.ucoach.models

/**
 * Created by Jan Herec on 2.9.2017.
 */
object ParticipantUtils {
    /**
     * Funkce vrátí křestní jméno z celého jména
     */
    fun getFirstNameFromFullName(fullName : String?) : String? {
        fullName?.let {
            if (fullName.split("\\w+".toRegex()).size > 1) {
                return fullName.substring(0, fullName.lastIndexOf(' '))
            } else {
                return fullName;
            }
        }

        return null
    }

    /**
     * Funkce vrátí příjemní z celého jména
     */
    fun getLastNameFromFullName(fullName : String?) : String? {
        fullName?.let {
            if (fullName.split("\\w+".toRegex()).size > 1) {
                return fullName.substring(fullName.lastIndexOf(" ") + 1)
            }
        }

        return null
    }
}