package net.raiper34.ucoach.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import net.raiper34.ucoach.R
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import net.raiper34.ucoach.dataClasses.Rate
import net.raiper34.ucoach.enums.Feeling
import net.raiper34.ucoach.models.GraphDateValueFormater
import java.util.*

/**
 * Graph fragment class
 * @requiredParameters: PARAM_GOAL_ID
 * @description: draw graph with database data of given goal
 * @author: Filip Gulan
 */
class GraphFragment : Fragment() {

    //Paths
    private val ROOT_PATH = ""
    private val RATES_PATH = "rates"

    //Params
    private val PARAM_GOAL_ID = "goalId"

    //Times
    private val TOAST_TIME_ERROR = 5000

    //Graph settings
    private val GRAPH_LINE_WIDTH = 5f
    private val GRAPH_DOT_RADIUS = 5f
    private val GRAPH_LINE_DASH = 30f
    private val GRAPH_ONE_DAY = 86400000
    private val GRAPH_COLOR = -43230

    //Attributes
    private var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    private var goalId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            goalId = arguments.getString(PARAM_GOAL_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_graph, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        val dbRef = database.getReference(ROOT_PATH)

        //Basic setup of graph
        val chart = view?.findViewById<LineChart>(R.id.chart) as LineChart
        chart.legend.setEnabled(false)
        chart.description.text = ""
        chart.axisLeft.setDrawLabels(false)
        chart.axisRight.setDrawLabels(false)
        chart.xAxis.setDrawLabels(false)
        chart.xAxis.setDrawGridLines(false)
        chart.axisLeft.setDrawGridLines(false)
        chart.axisRight.setDrawGridLines(false)

        //Get rates from firebase
        dbRef.child(RATES_PATH).child(goalId).orderByChild("date").addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {

                        //Fill temp array
                        val rates = ArrayList<Rate>()
                        dataSnapshot.children.forEach { i ->
                            val rate: Rate = i.getValue(Rate::class.java)
                            rates.add(rate)
                        }
                        val entries = createGraphData(rates)

                        if(entries.size > 1) {
                            //Set up dataset
                            val dataSet = LineDataSet(entries, "")
                            dataSet.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                            dataSet.lineWidth = GRAPH_LINE_WIDTH
                            dataSet.color = GRAPH_COLOR
                            dataSet.setDrawValues(false)
                            dataSet.setCircleColor(GRAPH_COLOR)
                            dataSet.circleRadius = GRAPH_DOT_RADIUS
                            dataSet.enableDashedLine(GRAPH_LINE_DASH, GRAPH_LINE_DASH / 3, GRAPH_LINE_DASH)
                            dataSet.setDrawFilled(true)
                            dataSet.fillColor = GRAPH_COLOR

                            //Draw graph
                            val lineData = LineData(dataSet)
                            chart.data = lineData
                            chart.setDrawMarkers(false)
                            chart.invalidate()

                            chart.data.setValueFormatter(GraphDateValueFormater())
                            chart.setVisibleXRangeMaximum(GRAPH_ONE_DAY.toFloat() * 7) //important to be here after data fill
                            chart.moveViewToX(Date().time.toFloat())
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Toast.makeText(activity.applicationContext, "Check your internet connection!", TOAST_TIME_ERROR).show()
                    }
                }
        )
    }

    /**
     * Create graph data
     * Create graph data from datas from database
     */
    private fun createGraphData(rates: ArrayList<Rate>): ArrayList<Entry> {
        val entries = ArrayList<Entry>()
        val rawEntries = ArrayList<Feeling>()
        if (rates.size > 1) {
            //Every value except last
            var index = 0
            while (index <= rates.size - 2) {
                val value = rates[index].date
                if (entries.size > 0 && entries[entries.size - 1].x == value?.toFloat()) { //if mutliple rates at same day, we squash it to one
                    entries[entries.size - 1].y = (entries[entries.size - 1].y + countValue(rawEntries, rates[index].feeling as Feeling)) / 2
                    rawEntries[rawEntries.size - 1] = this.averageFeeling(rawEntries[rawEntries.size - 1], rates[index].feeling as Feeling)
                } else {
                    entries.add(Entry(value?.toFloat() as Float, countValue(rawEntries, rates[index].feeling as Feeling)))
                    rawEntries.add(rates[index].feeling as Feeling)
                }
                var iterator = value
                var i = 0
                while (iterator < (rates[index + 1].date as Long)) { //create 0 value feeling days
                    if (i != 0) {
                        entries.add(Entry(iterator.toFloat(), countValue(rawEntries, Feeling.WORST)))
                        rawEntries.add(Feeling.WORST)
                    }
                    iterator += GRAPH_ONE_DAY
                    i++
                }
                index++
            }
            //Last value
            val value = rates[rates.size - 1].date
            entries.add(Entry(value?.toFloat() as Float, countValue(rawEntries, rates[rates.size - 1].feeling as Feeling)))
            rawEntries.add(rates[index].feeling as Feeling)
            var iterator = value
            var i = 0
            while (iterator < Date().time) { //create 0 value feeling days
                if (i != 0) {
                    entries.add(Entry(iterator.toFloat(), countValue(rawEntries, Feeling.WORST)))
                    rawEntries.add(Feeling.WORST)
                }
                iterator += GRAPH_ONE_DAY
                i++
            }
        }
        return entries
    }

    private fun averageFeeling(feeling1: Feeling, feeling2: Feeling): Feeling {
        if (feeling1 === Feeling.AVERAGE || feeling2 === Feeling.AVERAGE) {
            if (feeling1 === Feeling.WORST || feeling2 === Feeling.WORST) {
                return Feeling.BAD
            }
            else if (feeling1 === Feeling.BEST || feeling2 === Feeling.BEST) {
                return Feeling.GOOD
            }
            else  {
                return Feeling.AVERAGE
            }
        } else if (feeling1 === Feeling.BEST || feeling2 === Feeling.BEST) {
            if (feeling1 === Feeling.WORST || feeling2 === Feeling.WORST) {
                return Feeling.AVERAGE
            }
            else if (feeling1 === Feeling.BAD || feeling2 === Feeling.BAD) {
                return Feeling.AVERAGE
            }
            else  {
                return Feeling.BEST
            }
        } else if (feeling1 === Feeling.WORST || feeling2 === Feeling.WORST) {
            if (feeling1 === Feeling.GOOD || feeling2 === Feeling.GOOD) {
                return Feeling.AVERAGE
            }
            else  {
                return Feeling.WORST
            }
        }
        return Feeling.AVERAGE
    }

    /**
     * Count value
     * Count current value of day for graph
     */
    private fun countValue(entries: ArrayList<Feeling>, feeling: Feeling): Float {
        var feelingValue = this.getFeelingValue(feeling)
        when (entries.size) {
            0 -> feelingValue = feelingValue
            1 -> feelingValue += this.getFeelingValue(entries[entries.size - 1])
            2 -> feelingValue += this.getFeelingValue(entries[entries.size - 1]) +
                    this.getFeelingValue(entries[entries.size - 2])
            3 -> feelingValue += this.getFeelingValue(entries[entries.size - 1]) +
                    this.getFeelingValue(entries[entries.size - 2]) +
                    this.getFeelingValue(entries[entries.size - 3])
            4 -> feelingValue += this.getFeelingValue(entries[entries.size - 1]) +
                    this.getFeelingValue(entries[entries.size - 2]) +
                    this.getFeelingValue(entries[entries.size - 3]) +
                    this.getFeelingValue(entries[entries.size - 4])
            else -> feelingValue += this.getFeelingValue(entries[entries.size - 1]) +
                    this.getFeelingValue(entries[entries.size - 2]) +
                    this.getFeelingValue(entries[entries.size - 3]) +
                    this.getFeelingValue(entries[entries.size - 4]) +
                    this.getFeelingValue(entries[entries.size - 5])
        }
        if (feelingValue < 0) {
            return 0f
        }
        return  feelingValue
    }

    /**
     * Get feeling  value
     * Get value of feeling, feeling is string enum, we want number
     */
    private fun getFeelingValue(feeling: Feeling): Float {
        when (feeling) {
            Feeling.BEST -> return 2f
            Feeling.GOOD -> return 1f
            Feeling.AVERAGE -> return 0f
            Feeling.BAD -> return -1f
            Feeling.WORST -> return -2f
        }
    }


    companion object {

        private val GOAL_ID = "goalId"

        fun newInstance(goalId: String?): GraphFragment {
            val fragment = GraphFragment()
            val args = Bundle()
            args.putString(GOAL_ID, goalId)
            fragment.arguments = args
            return fragment
        }
    }

}