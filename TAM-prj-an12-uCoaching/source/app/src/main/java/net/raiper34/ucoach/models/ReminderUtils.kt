package net.raiper34.ucoach.models

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Button
import net.raiper34.ucoach.AlarmReceiver
import net.raiper34.ucoach.R
import net.raiper34.ucoach.dataClasses.Reminder
import android.widget.Toast
import net.raiper34.ucoach.dataClasses.Goal
import net.raiper34.ucoach.dataClasses.Participant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*
import java.text.SimpleDateFormat


/**
 * Created by Jan Herec on 30.09.2017.
 */
object ReminderUtils {

    val TAG = "55"

    /**
     * Na základě stringu kterým je název dne v týdnu vrátí integerovskou konstantu dne v týdnu kompatibilní s konstantami ve tříde Calendar
     */
    fun getCalendarWeekDayConstantFromString(dayOfWeek: String) : Int {
        when(dayOfWeek) {
            "MONDAY" -> {
                return Calendar.MONDAY
            }
            "TUESDAY" -> {
                return Calendar.TUESDAY
            }
            "WEDNESDAY" -> {
                return Calendar.WEDNESDAY
            }
            "THURSDAY" -> {
                return Calendar.THURSDAY
            }
            "FRIDAY" -> {
                return Calendar.FRIDAY
            }
            "SATURDAY" -> {
                return Calendar.SATURDAY
            }
            "SUNDAY" -> {
                return Calendar.SUNDAY
            }
            else -> {
                Log.e(TAG, "getCalendarWeekDayConstantFromString ERROR")
                throw Exception("getCalendarWeekDayConstantFromString ERROR")
            }
        }
    }

    /**
     * Funkce vytvoří nebo updatuje alarm v alarm manageru, tedy v systému Android, tento alarm při spuštění vyvolá notifikaci, která představuje reminder
     */
    fun createOrUpdateAlarmInAlarmManagerForReminderNotification(reminder : Reminder, context : Context) {
        val mFirebaseDatabase = FirebaseDatabase.getInstance().reference

        // postupne pro uzivatelem vybrane dny v tydnu nastavime alarm s tydennim opakovanim
        for (i in 1..7) {
            var alarmId = 0
            var dayOfWeek = 0
            // nastavime podle dne v tydnu potrebne promenne
            when(i) {
                1 ->  {
                    alarmId = reminder.monday!!.toInt()
                    dayOfWeek = Calendar.MONDAY
                }
                2 ->  {
                    alarmId = reminder.tuesday!!.toInt()
                    dayOfWeek = Calendar.TUESDAY
                }
                3 ->  {
                    alarmId = reminder.wednesday!!.toInt()
                    dayOfWeek = Calendar.WEDNESDAY
                }
                4 ->  {
                    alarmId = reminder.thursday!!.toInt()
                    dayOfWeek = Calendar.THURSDAY
                }
                5 ->  {
                    alarmId = reminder.friday!!.toInt()
                    dayOfWeek = Calendar.FRIDAY
                }
                6 ->  {
                    alarmId = reminder.saturday!!.toInt()
                    dayOfWeek = Calendar.SATURDAY
                }
                7 ->  {
                    alarmId = reminder.sunday!!.toInt()
                    dayOfWeek = Calendar.SUNDAY
                }
            }

            // pokud je id alarmu mensi nez 0 (typicky -1), tak v dany den uzivatel nema alarm nastaven a tedy tento den vynechame
            if (alarmId < 0) {
                continue
            }

            // ---------- ZDE VYTVORIME ALARM, ALE NEJDRIV SI MUSIME Z FOREBASE DOTAHNOUT POTREBNA DATA KTERA SE PREDAJI ALARMU KDYZ SE SPUSTI -------------------------
            mFirebaseDatabase.child("goals").child(reminder.goalId).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // ziskame goal
                    val goal = dataSnapshot.getValue<Goal>(Goal::class.java)
                    val goalName = goal.name
                    mFirebaseDatabase.child("participants").child(goal.performer).addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            // ziskame performera
                            val participant = dataSnapshot.getValue<Participant>(Participant::class.java)
                            val fullName = participant.fullName

                            // ---------- ZDE VYTVORIME ALARM -------------------------
                            // zacneme s vytvarenim alarmu
                            val alarmIntent = Intent(context, AlarmReceiver::class.java)
                            // vlozime jako extras parametry notifikace
                            alarmIntent.putExtra("coachName", FirebaseAuth.getInstance().currentUser?.displayName)
                            alarmIntent.putExtra("goalId", reminder.goalId)
                            alarmIntent.putExtra("perfomerFullName", fullName)
                            alarmIntent.putExtra("goalName", goalName)

                            val pendingIntent = PendingIntent.getBroadcast(context, (alarmId).toInt(), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)

                            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

                            // nastavíme alarm na vybraný čas
                            val calendar = Calendar.getInstance()
                            calendar.setTimeInMillis(System.currentTimeMillis())
                            calendar.set(Calendar.HOUR_OF_DAY, reminder.hour!!.toInt())
                            calendar.set(Calendar.MINUTE, reminder.minute!!.toInt())

                            // najdeme si v kalendari nejbližší daný den v týdnu, který nastavujeme
                            while (calendar.get(Calendar.DAY_OF_WEEK) != dayOfWeek) {
                                calendar.add(Calendar.DATE, 1);
                            }

                            // casový interval mezi alarmy je implicitne jeden týden
                            var intervalBetweenAlarm = AlarmManager.INTERVAL_DAY * 7

                            // pokud je alarm nastavený do minulosti (tedy na dnesni den ale minuly cas), přidáme týden aby byl nastaven do budoucnosti
                            if (calendar.timeInMillis < System.currentTimeMillis()) {
                                calendar.add(Calendar.DATE, 7)
                            }

                            // nakonec nastavíme alarm aby se od daného daného dne opakoval bud denně nebo jednou za týden, podle toho co si vybral uživate
                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                                    intervalBetweenAlarm, pendingIntent)
                        }

                        override fun onCancelled(databaseError: DatabaseError) {
                            Toast.makeText(context, "Error. Cannot load participant performer from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                            Log.e(TAG, databaseError.message)
                        }
                    })
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(context, "Error. Cannot load goal from db. ${databaseError.message}", Toast.LENGTH_SHORT).show()
                    Log.e(TAG, databaseError.message)
                }
            })
        }
    }

    /**
     * Funkce odstraní alarm v alarm manageru, tedy v systému Android, tento alarm při spuštění vyvolá notifikaci, která představuje reminder
     */
    fun removeAlarmInAlarmManagerForReminderNotification(reminder : Reminder, context : Context) {
        // postupne pro vsechny dny v tydnu alarm zrusime
        for (i in 1..7) {
            var alarmId = 0
            // nastavime podle dne v tydnu id alarmu
            when(i) {
                1 ->  {
                    alarmId = reminder.monday!!.toInt()
                }
                2 ->  {
                    alarmId = reminder.tuesday!!.toInt()
                }
                3 ->  {
                    alarmId = reminder.wednesday!!.toInt()
                }
                4 ->  {
                    alarmId = reminder.thursday!!.toInt()
                }
                5 ->  {
                    alarmId = reminder.friday!!.toInt()
                }
                6 ->  {
                    alarmId = reminder.saturday!!.toInt()
                }
                7 ->  {
                    alarmId = reminder.sunday!!.toInt()
                }
            }

            // pokud je id alarmu mensi nez 0 (typicky -1), tak v dany den uzivatel nema alarm nastaven a tedy tento den vynechame z mazani
            if (alarmId < 0) {
                continue
            }

            // smazeme existujici alarm
            val alarmIntent = Intent(context, AlarmReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, alarmId, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
        }
    }

    /**
     * Funkce na základe hodina  minut ve formatu 24, vraci list kde prvnim prvek je normalizovany cas v americkem formatu a fruhym prvkem je indikator AM resp PM
     */
    fun getAmPmTimeFromHoursAndMinutes(hours: Long, minutes : Long) : List<String> {
        var amPm = "AM"
        var hours = hours.toInt()
        if (hours > 12) {
            hours = hours - 12
            amPm = "PM"
        }
        return listOf(hours.toString() + ":" + minutes.toString(), amPm)
    }

    /**
     * Funkce z retezce obsahujici cas tento ziska a vrati nastaveny kalendar na tento cas
     */
    fun getCalendarTimeFromString(timeInString : String) : Calendar {
        val formatter = SimpleDateFormat("h:m a")
        val date = formatter.parse(timeInString)
        val calendar = Calendar.getInstance()
        calendar.time = date
        return calendar
    }

    /**
     * Funkce zjistije pro dany button reprezentujici den v tydnu jestli je aktivni a pokud ano vaci navrhnute reminder id
     * Jinak vraci -1
     */
    fun getReminderId(button : Button, suggestedReminderId : Int) : Long {
        val isActive = button.getTag(R.id.TAG_REMINDER_IN_DAY_IS_SET) as Boolean
        if (isActive) {
            return suggestedReminderId.toLong()
        }
        else {
            return -1
        }
    }
}