package net.raiper34.ucoach.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import net.raiper34.ucoach.R
import com.google.firebase.auth.FirebaseAuth
import net.raiper34.ucoach.MyAuthStateListener
import net.raiper34.ucoach.OnApplicationLaunch

class MainActivity : AppCompatActivity() {
    // Firebase instance variables
    private var mFirebaseAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        // create notification channels
        OnApplicationLaunch.createNotificationChannels(this@MainActivity)

        mFirebaseAuth = FirebaseAuth.getInstance()
        mAuthListener = MyAuthStateListener(this)
    }

    override fun onStart() {
        super.onStart()
        mAuthListener?.let { mFirebaseAuth?.addAuthStateListener(it) }
    }

    override fun onStop() {
        super.onStop()
        mAuthListener?.let { mFirebaseAuth?.removeAuthStateListener(it) }
    }

}
