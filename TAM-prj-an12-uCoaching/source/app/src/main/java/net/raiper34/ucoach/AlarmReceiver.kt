package net.raiper34.ucoach

import android.app.Notification
import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.app.NotificationManager
import android.app.PendingIntent
import net.raiper34.ucoach.activities.CoachActivity
import android.graphics.BitmapFactory




/**
 * Created by Jan Herec on 30.09.2017.
 */
class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        // show toast
        Toast.makeText(context, "Alarm running", Toast.LENGTH_SHORT).show()

        val coachName = intent.getStringExtra("coachName")
        val goalId = intent.getStringExtra("goalId")
        val perfomerFullName = intent.getStringExtra("perfomerFullName")
        val goalName = intent.getStringExtra("goalName")

        val resourceId = R.drawable.ic_launcher_web
        val icon = BitmapFactory.decodeResource(context.getResources(), resourceId)

        // SHOW NOTIFICATION
        val builder = NotificationCompat.Builder(context, "channel_coach_reminder")
                .setSmallIcon(R.drawable.ic_add_alarm_black_24dp)
                //.setStyle(NotificationCompat.BigTextStyle().bigText("Hi $coachName, It's time to coach your performer $perfomerFullName within his goal '$goalName'"))
                .setContentTitle("uCoaching reminder")
                .setContentText("Hi $coachName, it's time to coach $perfomerFullName")
                .setSubText("within his goal '$goalName'")
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setVisibility(Notification.VISIBILITY_PUBLIC)

        val notificationIntent = Intent(context, CoachActivity::class.java)
        val contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        builder.setContentIntent(contentIntent)

        // Add as notification
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(0, builder.build())
    }
}