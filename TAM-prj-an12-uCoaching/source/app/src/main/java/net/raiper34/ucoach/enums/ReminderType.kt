package net.raiper34.ucoach.enums

/**
 * Created by Filip Gulanb on 26.8.2017.
 * Edited by Jan Herec
 */

enum class ReminderType {
    DAILY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
