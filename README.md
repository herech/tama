# TAMa

This Android app is platform for micro-coaching.

- Two app modes: performer and coach
- Performer is pushed towards by coach (mostly his friend) to achieve his goal
- Programmed in Kotlin, use Firebase Realtime DB and Firebase Cloud Messaging.
